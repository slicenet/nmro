package edu.upc.gco.slcnt.orch.nmro.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AESTest {

	private static final Logger log = LoggerFactory.getLogger(AESTest.class);
	private static SecretKeySpec secretKey;
	private static byte[] key;
	private static String encoding = "ASCII";//"UTF-8"
	
	public static void setKey (String myKey)
	{
		MessageDigest sha = null;
		
		try {
			key = myKey.getBytes(encoding);
			//sha = MessageDigest.getInstance("SHA-1");
			//key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public static void setKey (byte[] myKey)
	{
		key = Arrays.copyOf(myKey, 32);
		secretKey = new SecretKeySpec(key, "AES");
	}
	
	public static void setKey (String myKey, String salt)
	{
		try {
			if (myKey.equals(""))
				key = new byte[32];
			else
				//key = myKey.getBytes("encoding");
				key = Base64.getDecoder().decode(myKey);
			
			byte[] saltBytes = null;
		
			saltBytes = salt.getBytes(encoding);
			//saltBytes = Base64.getDecoder().decode(salt);
			
			byte[] key32 = new byte[32];
			for (int i=0; i<key32.length; i++)
			{
				if (i < key.length)
					key32[i] = key[i];
				else
					key32[i] = 0;
			}
			
			for (int i=0; i<saltBytes.length; i++)
			{
				key32[i%32] ^= saltBytes[i];
			}
			
			
			
			secretKey = new SecretKeySpec(key32, "AES");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// commonKey => getBytes
	public static byte[] joinCommonKey (String commonKey)
	{
		byte[] newSecretKeyBytes = new byte[32];
		byte[] commonKeyBytes = commonKey.getBytes();//Base64.getDecoder().decode(commonKey);
		
		for (int i=0; i<commonKeyBytes.length; i++)
			newSecretKeyBytes[i%32] ^= commonKeyBytes[i];
		
		return newSecretKeyBytes;
	}
	
	public static byte[] joinSecretKey (byte[] updateKey, byte[] secretKey)
	{
		byte[] newSecretKey = null;
		
		if (updateKey == null)
			return secretKey;
		
		if (secretKey == null)
			newSecretKey = new byte[32];
		else
			newSecretKey = Arrays.copyOf(secretKey, /*secretKey.length*/32);
		
		log.info("[joinSecretKey] length = " + updateKey.length);
		for (int i=0; i < updateKey.length; i++)
		{
			//log.info("[joinSecretKey] i = " + i + ", newSecretKey[i%32] = " + newSecretKey[i%32] + ", updateKey[i] = " + updateKey[i]);
			newSecretKey[i%32] ^= updateKey[i];
		}
		return newSecretKey;
	}
	
	// updateKey => Base64, secretKey => getBytes
	public static byte[] joinSecretKey0 (String updateKey, String secretKey)
	{
		byte[] newSecretKeyBytes = null;
		
		//try
		//{
			if (updateKey.equals(""))
				return secretKey.getBytes(/*encoding*/);
				//return Base64.getDecoder().decode(secretKey);
			
			//byte[] updateKeyBytes = updateKey.getBytes(/*encoding*/);
			byte[] updateKeyBytes = Base64.getDecoder().decode(updateKey);
			
			if (secretKey.equals(""))
				newSecretKeyBytes = new byte[32];
			else
			{
				//newSecretKeyBytes = secretKey.getBytes(/*encoding*/);
				newSecretKeyBytes = Base64.getDecoder().decode(secretKey);
				if (newSecretKeyBytes.length < 32)
					newSecretKeyBytes = Arrays.copyOf(newSecretKeyBytes, 32);
			}
			
			for (int i=0; i<updateKeyBytes.length; i++)
				newSecretKeyBytes[i%32] ^= updateKeyBytes[i];
		/*}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return newSecretKeyBytes;
	}
		
	// updateKey => getBytes, secretKey => getBytes
	public static byte[] joinSecretKey1 (String updateKey, String secretKey)
	{
		byte[] newSecretKeyBytes = null;
		
		//try
		//{
			if (updateKey.equals(""))
				return secretKey.getBytes(/*encoding*/);
				//return Base64.getDecoder().decode(secretKey);
			
			byte[] updateKeyBytes = updateKey.getBytes(/*encoding*/);
			//byte[] updateKeyBytes = Base64.getDecoder().decode(updateKey);
			
			if (secretKey.equals(""))
				newSecretKeyBytes = new byte[32];
			else
				newSecretKeyBytes = secretKey.getBytes(/*encoding*/);
				//newSecretKeyBytes = Base64.getDecoder().decode(secretKey);
			
			for (int i=0; i<updateKeyBytes.length; i++)
				newSecretKeyBytes[i%32] ^= updateKeyBytes[i];
		/*}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return newSecretKeyBytes;
	}
	// updateKey => getBytes, secretKey => Base64
	public static byte[] joinSecretKey2 (String updateKey, String secretKey)
	{
		byte[] newSecretKeyBytes = null;
		
		//try
		//{
			if (updateKey.equals(""))
				//return secretKey.getBytes(encoding);
				return Base64.getDecoder().decode(secretKey);
			
			byte[] updateKeyBytes = updateKey.getBytes(/*encoding*/);
			//byte[] updateKeyBytes = Base64.getDecoder().decode(updateKey);
			
			if (secretKey.equals(""))
				newSecretKeyBytes = new byte[32];
			else
				//newSecretKeyBytes = secretKey.getBytes(encoding);
				newSecretKeyBytes = Base64.getDecoder().decode(secretKey);
			
			for (int i=0; i<updateKeyBytes.length; i++)
				newSecretKeyBytes[i%32] ^= updateKeyBytes[i];
		/*}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return newSecretKeyBytes;
	}
	
	private static String manualZeroPadding (String inputStr)
	{
		String outputStr = inputStr;
		
		int size = 16 - inputStr.length();
		for (; size > 0; size--)
			outputStr += '\0';
		
		return outputStr;
	}
	
	private static void printBytesHex (String caption, byte[] bytes)
	{
		String byteStr = "";
		for (byte b : bytes)
			byteStr += String.format("%02x", b);
		log.info(caption + byteStr);
	}
	
	private static void printBytesChar (String caption, byte[] bytes)
	{
		String byteStr = "";
		for (byte b : bytes)
			byteStr += String.format("%c", b);
		log.info(caption + byteStr);
	}
	
	private static String serialToBase64String (String serial)
	{
		byte[] bSerial = Base64.getDecoder().decode(serial);
		String serialStr = "";
		/*for (byte b : bSerial)
			serialStr += String.format("%c", b);*/
		serialStr = new String(bSerial);
		
		return serialStr;
	}
	
	public static String totalEncrypt (String strToEncrypt, String serial, String commonKey, String salt)
	{
		// Compute first salt from commonKey
		byte[] firstSalt = joinCommonKey(commonKey);
		printBytesChar ("[totalEncrypt] firstSalt = ", firstSalt);
		
		String serialB64 = serialToBase64String (serial);
		byte[] bSecret = joinSecretKey (Base64.getDecoder().decode(serialB64), firstSalt);
		//printBytesChar ("[totalEncrypt] bSecret = ", bSecret);
		
		return encrypt (strToEncrypt, bSecret, salt.getBytes());
	}
	
	public static String totalDecrypt (String strToDecrypt, String serial, String commonKey, String salt)
	{
		// Compute first salt from commonKey
		byte[] firstSalt = joinCommonKey(commonKey);
		printBytesChar ("[totalEncrypt] firstSalt = ", firstSalt);
		
		String serialB64 = serialToBase64String (serial);
		byte[] bSecret = joinSecretKey (Base64.getDecoder().decode(serialB64), firstSalt);
		//printBytesChar ("[totalEncrypt] bSecret = ", bSecret);
		
		return decrypt (strToDecrypt, bSecret, salt.getBytes());
	}
	
	public static String encrypt (String strToEncrypt, byte[] secretKeyBytes, byte[] salt)
	{
		//byte[] newSecretKeyBytes = joinSecretKey1((salt==null)?"":new String(salt), ((secretKeyBytes==null)?"":new String(secretKeyBytes)));//joinSecretKey (salt, secretKeyBytes);
		byte[] newSecretKeyBytes = joinSecretKey(salt, secretKeyBytes);
		printBytesHex ("[encrypt] newSecretBytes = ", newSecretKeyBytes);
		
		setKey(newSecretKeyBytes);
		
		try {
			//Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			//return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
			String strToEncryptPadding = manualZeroPadding(strToEncrypt);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncryptPadding.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		/*} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();*/
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String decrypt (String strToDecrypt, byte[] secretKeyBytes, byte[] salt)
	{
		byte[] newSecretKeyBytes = joinSecretKey(salt, secretKeyBytes);
		
		setKey(newSecretKeyBytes);
		
		try {
			//Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			//String decryptedStr = new String (cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
			String decryptedStr = new String (cipher.doFinal(Base64.getDecoder().decode(strToDecrypt))).trim();//stripTrailing();
			return decryptedStr;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static String encrypt (String strToEncrypt, String secret, String salt)
	{
		if (salt.equals(""))
			setKey(secret);
		else
			setKey(secret,salt);
			
		try {
			//Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			//return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
			String strToEncryptPadding = manualZeroPadding(strToEncrypt);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncryptPadding.getBytes(encoding)));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static String decrypt (String strToDecrypt, String secret, String salt)
	{
		if (salt.equals(""))
			setKey(secret);
		else
			setKey(secret,salt);
		try {
			//Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			//String decryptedStr = new String (cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
			String decryptedStr = new String (cipher.doFinal(Base64.getDecoder().decode(strToDecrypt))).trim();//stripTrailing();
			return decryptedStr;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
