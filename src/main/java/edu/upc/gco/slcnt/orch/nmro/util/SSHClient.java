package edu.upc.gco.slcnt.orch.nmro.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SSHClient {

	private Logger log = LoggerFactory.getLogger(SSHClient.class);
	private JSch jsch;
	private Session session;
	
	public SSHClient ()
	{
		jsch = new JSch();
	}
	
	public void connect (String user, String hostIP, int port, String password)
	{
		try {
			session = jsch.getSession(user, hostIP, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void disconnect ()
	{
		session.disconnect();
	}
	
	public String executeCommand (String command)
	{
		StringBuilder outputBuffer = new StringBuilder();
		
		Channel channel;
		try {
			channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command);
			InputStream commandOutput = channel.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(commandOutput));
			channel.connect();
			
			String line;
			while((line = reader.readLine()) != null)
				outputBuffer.append(line+"\n");
			reader.close();
			
			channel.disconnect();
		} catch (JSchException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return outputBuffer.toString();
	}
	
	public String buildCommand (String cmdName, List<String> params)
	{
		switch (cmdName)
		{
			case "HssStatus":
				return "/snap/bin/oai-cn.hss-status ";
			case "AddIMSI":
				return "/snap/bin/oai-cn.hss-add-user " + params.get(0);
			case "StressCirros":
				return "TODO - Put Command";
			default:
				return null;
		}
	}
}
