package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmDriver;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstance;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.VM;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.VNF;

@Service
public class NmroNsInventoryService {

	private static final Logger log = LoggerFactory.getLogger(NmroNsInventoryService.class);
	
	private Nmro nmro;
	private OsmDriver osmDriver;
	
	public NmroNsInventoryService () { }
	
	public void initService (Nmro nmro)
	{
		this.nmro = nmro;
		this.osmDriver = nmro.getOsmDriver();
	}
	
	public List<String> getNsiResources (String nsiId)
	{
		List<String> resourceIds = new ArrayList<String>();
		
		ResponseEntity<NSInstance> response = (ResponseEntity<NSInstance>) osmDriver.getOsmLcm().getNsiInfo(nsiId);
		NSInstance nsi = response.getBody();
		for (VNF vnf : nsi.getDeploymentStatus().getVnfs())
		{
			for (VM vm : vnf.getVms())
			{
				resourceIds.add(vm.getVimVmId());
			}
		}
		
		return resourceIds;
	}
	
	public List<String> getNsiClients (String nsiId)
	{
		List<String> clientIPs = new ArrayList<String>();
		
		ResponseEntity<NSInstance> response = (ResponseEntity<NSInstance>) osmDriver.getOsmLcm().getNsiInfo(nsiId);
		NSInstance nsi = response.getBody();
		for (VNF vnf : nsi.getDeploymentStatus().getVnfs())
		{
			clientIPs.add(Arrays.asList(vnf.getIpAddress().split(";")).get(0));
		}
		
		return clientIPs;
	}
}
