package edu.upc.gco.slcnt.orch.nmro.osm;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import edu.upc.gco.slcnt.rest.client.osm.model.catalogue.nsDescriptor.NSDescriptorEx;
import edu.upc.gco.slcnt.rest.client.osm.model.catalogue.vnfDescriptor.VNFDescriptorEx;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstance;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.elements.NsdInfo;
import it.nextworks.nfvmano.libs.ifa.common.elements.MonitoringParameter;
import it.nextworks.nfvmano.libs.ifa.common.elements.VirtualCpuData;
import it.nextworks.nfvmano.libs.ifa.common.elements.VirtualMemoryData;
import it.nextworks.nfvmano.libs.ifa.common.enums.CpRole;
import it.nextworks.nfvmano.libs.ifa.common.enums.InstantiationState;
import it.nextworks.nfvmano.libs.ifa.common.enums.LayerProtocol;
import it.nextworks.nfvmano.libs.ifa.common.enums.OperationalState;
import it.nextworks.nfvmano.libs.ifa.common.enums.UsageState;
import it.nextworks.nfvmano.libs.ifa.descriptors.common.elements.ConnectivityType;
import it.nextworks.nfvmano.libs.ifa.descriptors.common.elements.SwImageDesc;
import it.nextworks.nfvmano.libs.ifa.descriptors.common.elements.VirtualComputeDesc;
import it.nextworks.nfvmano.libs.ifa.descriptors.common.elements.VirtualStorageDesc;
import it.nextworks.nfvmano.libs.ifa.descriptors.onboardedvnfpackage.OnboardedVnfPkgInfo;
import it.nextworks.nfvmano.libs.ifa.descriptors.onboardedvnfpackage.SoftwareImageInformation;
import it.nextworks.nfvmano.libs.ifa.descriptors.onboardedvnfpackage.VnfPackageArtifactInformation;
import it.nextworks.nfvmano.libs.ifa.descriptors.onboardedvnfpackage.VnfPackageSoftwareImageInformation;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.Vdu;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.VduCpd;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.VnfDf;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.VnfExtCpd;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.VnfIndicator;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.VnfVirtualLinkDesc;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.VnfcConfigurableProperties;
import it.nextworks.nfvmano.libs.ifa.descriptors.vnfd.Vnfd;
import it.nextworks.nfvmano.libs.ifa.records.nsinfo.NsInfo;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.nsDescriptor.ConstituentVNFD;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.ConnectionPoint;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.Interface;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.InternalConnectionPoint;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.InternalVld;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.VDU;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.Volumes;

@Component
public class OsmTranslator {

	private static final Logger log = LoggerFactory.getLogger(OsmTranslator.class);
	
	// Translate Objects from OSM (SOL005) to NMR-O (IFA013)
	
	// Map NsInstance into NsInfo
	private NsInfo nsiFromOsmToNmro (NSInstance objIn)
	{
		NsInfo nsInfo = new NsInfo(objIn.getId(),
				objIn.getName(),
				objIn.getDescription(), 
				objIn.getNsdId(),
				"",
				objIn.getVnfdIdList(),
				null,
				objIn.getAdminObject().getNsState().equals("INSTANTIATED") ? InstantiationState.INSTANTIATED : InstantiationState.NOT_INSTANTIATED,
				null,
				null,
				"tenantId",
				null);
		
		return nsInfo;
	}
	
	// TODO: Delete - Map NSDescriptor into NsdInfo
	private NsdInfo nsdFromOsmToNmro_notUsed (NSDescriptorEx objIn)
	{
		NsdInfo nsdInfo = new NsdInfo(objIn.getNsdId(),
				objIn.getNsdId(),
				objIn.getName(),
				objIn.getVersion(),
				objIn.getVendor(),
				null,
				null,
				null,
				"",
				objIn.getAdminInfo().getOperationalState().equals("ENABLED") ? OperationalState.ENABLED:OperationalState.DISABLED,
				objIn.getAdminInfo().getUsageState().equals("IN_USE") ? UsageState.IN_USE:UsageState.NOT_IN_USE,
				false,
				null);
		
		return nsdInfo;
	}
	
	private VirtualStorageDesc storageFromOsmToNmro (int storageId, Volumes osmStorage)
	{
		// I assume that Volume refers to a Block Storage (OpenStack CINDER)
		VirtualStorageDesc nmroStorage = new VirtualStorageDesc("VirtualStorageDesc-" + storageId + "_" + osmStorage.getName(), "VOLUME", osmStorage.getSize(), false, osmStorage.getImage());
		
		return nmroStorage;
	}
	
	// Assumptions for Storage (taken from OpenStack: Ephemeral (NOVA) (Default), Block (CINDER), File (MANILA), Object (SWIFT) 
	private List<VirtualStorageDesc> storageListFromOsmToNmro (List<VDU> osmVduList)
	{
		List<VirtualStorageDesc> nmroStorageList = new ArrayList<VirtualStorageDesc>();
		
		if (osmVduList == null)
			return nmroStorageList;
		
		int storageId = 0;
		for (VDU osmVdu : osmVduList)
		{
			if (osmVdu.getVolumes() == null)
			{
				// I assume that if no Volumes available the default OpenStack NOVA storage (Ephemeral)
				VirtualStorageDesc nmroStorage = new VirtualStorageDesc("VirtualStorageDesc-" + (++storageId), "EPHEMERAL", osmVdu.getVmFlavor().getStorageGb(), false, osmVdu.getImage());
				nmroStorageList.add(nmroStorage);
			}
			else
			{
				for (Volumes volume : osmVdu.getVolumes())
				{
					VirtualStorageDesc nmroStorage = storageFromOsmToNmro (++storageId, volume);
					nmroStorageList.add(nmroStorage);
				}
			}
		}
		
		return nmroStorageList;
	}
	
	private void vnfdIntVirtualLinkDescListFromOsmToNmro (List<VnfVirtualLinkDesc> nmroIntVirtualLinkDesc, List<InternalVld> osmIntVldList, Vnfd nmroVnfd)
	{
		if (osmIntVldList == null)
			return;
		
		for (InternalVld intVld : osmIntVldList)
		{
			log.debug("Vld Type = " + intVld.getType());
			ConnectivityType ct = null;
			if (intVld.getType() == null)
				ct = new ConnectivityType(LayerProtocol.NOT_SPECIFIED, "no flowPattern");
			List<String> testAccess = new ArrayList<String>();
			List<MonitoringParameter> monitoringParams = new ArrayList<MonitoringParameter>();
			VnfVirtualLinkDesc vnfVld = new VnfVirtualLinkDesc(nmroVnfd, intVld.getId(), ct, testAccess, intVld.getDescription(), monitoringParams);
			nmroIntVirtualLinkDesc.add(vnfVld);
		}
	}
	
	private InternalVld getIntVldAssociatedToCp (ConnectionPoint osmCp, List<InternalVld> osmIntVldList)
	{
		if (osmIntVldList == null)
			return null;
		
		for (InternalVld intVld : osmIntVldList)
		{
			log.debug("getIntVldAssociatedToCp: " + osmCp.getInternalVldRef() + " == " + intVld.getInternalConnectionPoint().getIdRef());
			if (osmCp.getInternalVldRef().equals(intVld.getInternalConnectionPoint().getIdRef()))
				return intVld;
		}
		
		return null;
	}
	
	private void vnfdExtCpdListFromOsmToNmro (List<VnfExtCpd> nmroExtCpList, VNFDescriptorEx vnfdEx, Vnfd nmroVnfd)
	{
		if ( vnfdEx.getConnectionPoints() == null)
			return;
		
		for (ConnectionPoint osmCp : vnfdEx.getConnectionPoints())
		{
			InternalVld intVld = getIntVldAssociatedToCp (osmCp, vnfdEx.getInternalVld());
			
			log.debug("CP Type = " + osmCp.getType());
			LayerProtocol layerProtocol = null;
			if (osmCp.getType() == null)
				layerProtocol = LayerProtocol.NOT_SPECIFIED;
			CpRole role = null;
			
			VnfExtCpd vnfExtCpd = null;
			if (intVld == null)
				vnfExtCpd = new VnfExtCpd(nmroVnfd, osmCp.getId(), layerProtocol, role, osmCp.getName(), null, null, null, null);
			else
				vnfExtCpd = new VnfExtCpd(nmroVnfd, osmCp.getId(), layerProtocol, role, osmCp.getName(), null, intVld.getId(), intVld.getInternalConnectionPoint().getIdRef(), null);
			
			nmroExtCpList.add(vnfExtCpd);
		}
	}
	
	private void virtualComputeDescListFromOsmToNmro (List<VirtualComputeDesc> nmroVirtualComputeDescList, List<VDU> osmVduList, Vnfd nmroVnfd)
	{
		int vcdIndex = 0;
		for (VDU osmVdu : osmVduList)
		{
			VirtualComputeDesc vcd = new VirtualComputeDesc(nmroVnfd, "virtualComputeDescId-" + (++vcdIndex), null, null, new VirtualCpuData("arch",osmVdu.getVmFlavor().getVcpuCount(),0,"none"), new VirtualMemoryData(osmVdu.getVmFlavor().getMemoryMb(),"none",false));
			nmroVirtualComputeDescList.add(vcd);
		}
	}
	
	private Interface getOsmInterfaceFromCp (String intCpRef, List<Interface> osmIfList)
	{
		if (osmIfList == null)
			return null;
		
		for (Interface osmIf : osmIfList)
		{
			log.debug("Interfaces: " + intCpRef + " == " + osmIf.getIntConnPointRef());
			if (osmIf.getIntConnPointRef().equals(intCpRef))
				return osmIf;
		}
		
		return null;
	}
	
	private void vduIntCpFromOsmToNmro (List<VduCpd> intCpdList, VDU osmVdu, Vdu nmroVdu)
	{
		if (osmVdu.getInternalConnectionPoints() == null)
			return;
		
		for (InternalConnectionPoint osmIntCp : osmVdu.getInternalConnectionPoints())
		{
			Interface osmIf = getOsmInterfaceFromCp(osmIntCp.getName(), osmVdu.getInterfaces());
			LayerProtocol layerProtocol = null;
			if (osmIntCp.getType() == null)
				layerProtocol = LayerProtocol.NOT_SPECIFIED;
			CpRole role = null;
			VduCpd vduCp = new VduCpd(nmroVdu, osmIntCp.getId(), layerProtocol, role, osmIntCp.getName(), null, osmIntCp.getInternalVldRef(), osmIf.getVirtualInterface().getBandwidth(), null);
			
			intCpdList.add(vduCp);
		}
	}
	
	private Vdu vduFromOsmToNmro (int vcdIndex, VDU osmVdu, Vnfd nmroVnfd)
	{
		//TODO: Complete the Translation
		
		List<String> virtualStorageDescList = new ArrayList<String>();
		
		SwImageDesc swImgDesc = new SwImageDesc(osmVdu.getImage(),
				osmVdu.getName(),
				"version - TODO",
				osmVdu.getImageChecksum(),
				"containerFormat - TODO",
				"diskFormat - TODO",
				osmVdu.getVmFlavor().getStorageGb().intValue(),
				osmVdu.getVmFlavor().getMemoryMb().intValue(),
				0,
				osmVdu.getImage(),
				"O.S. - TODO",
				"supportedVirtualisationEnvironment - TODO");
		
		List<String> nfviConstraintList = new ArrayList<String>();
		List<MonitoringParameter> monitoringParameterList = new ArrayList<MonitoringParameter>();
		VnfcConfigurableProperties configurableProperties = null;
		
		Vdu nmroVdu = new Vdu(nmroVnfd,
				osmVdu.getId(),
				osmVdu.getName(),
				osmVdu.getDescription(),
				"virtualComputeDescId-" + vcdIndex,
				virtualStorageDescList,
				null,
				swImgDesc,
				nfviConstraintList,
				monitoringParameterList,
				configurableProperties);
		
		vduIntCpFromOsmToNmro (nmroVdu.getIntCpd(), osmVdu, nmroVdu);
		
		return nmroVdu;
	}
	
	private void vduListFromOsmToNmro (List<Vdu> nmroVduList, List<VDU> osmVduList, Vnfd nmroVnfd)
	{
		if (osmVduList == null)
			return;
		
		int vcdIndex = 0;
		for (VDU osmVdu : osmVduList)
		{
			Vdu nmroVdu = vduFromOsmToNmro (++vcdIndex, osmVdu, nmroVnfd);
			nmroVduList.add(nmroVdu);
		}
	}
	
	private void vnfdDeploymentFlavourListFromOsmToNmro (List<VnfDf> nmroDeploymentFlavourList, List<VDU> osmVduList, Vnfd nmroVnfd)
	{
		if (osmVduList == null)
			return;
		
		for (VDU osmVdu : osmVduList)
		{
			VnfDf vnfDf = new VnfDf(nmroVnfd, osmVdu.getId() + "-flv", "flavourDescription = " + osmVdu.getVmFlavor().getVcpuCount() + " CPUs / " + osmVdu.getVmFlavor().getMemoryMb() + " Mb MEM / " + osmVdu.getVmFlavor().getStorageGb() + " Gb DISK", "defaultInstantiationLevel - not available", null, null, null, null);
			nmroDeploymentFlavourList.add(vnfDf);
		}
	}
	
	// Map VNFDescriptor into Vnfd. Used internally by vnfPkgFromOsmToNmro
	private Vnfd vnfdFromOsmToNmro (VNFDescriptorEx vnfdEx, OnboardedVnfPkgInfo onboardedVnfPkgInfo)
	{
		List<VirtualStorageDesc> nmroStorageList = storageListFromOsmToNmro(vnfdEx.getVduList());
		
		//TODO: Complete translation
		List<String> vnfmInfoList = new ArrayList<String>();
		List<String> localizationLanguageList = new ArrayList<String>();
		List<VnfIndicator> vnfIndicatorList = new ArrayList<VnfIndicator>();
		
		Vnfd vnfd = new Vnfd(onboardedVnfPkgInfo,
				vnfdEx.getId(),
				vnfdEx.getVendor(),
				"vnfProductName - TODO",
				"vnfSoftwareVersion - TODO",
				"vnfdVersion - TODO",
				"vnfProductInfoName - TODO",
				"vnfProductInfoDescription - TODO",
				vnfmInfoList,
				localizationLanguageList,
				"defaultLocalizationLanguage - TODO",
				nmroStorageList,
				vnfIndicatorList);
		
		vnfdIntVirtualLinkDescListFromOsmToNmro (vnfd.getIntVirtualLinkDesc(), vnfdEx.getInternalVld(), vnfd);
		vnfdExtCpdListFromOsmToNmro (vnfd.getVnfExtCpd(), vnfdEx, vnfd);
		vduListFromOsmToNmro(vnfd.getVdu(), vnfdEx.getVduList(), vnfd);
		//vnfd.setVdu(nmroVduList);
		vnfdDeploymentFlavourListFromOsmToNmro (vnfd.getDeploymentFlavour(), vnfdEx.getVduList(), vnfd);
		virtualComputeDescListFromOsmToNmro(vnfd.getVirtualComputeDesc(), vnfdEx.getVduList(), vnfd);
		
		return vnfd;
	}
	
	private void softwareImageListFromOsmToNmro (OnboardedVnfPkgInfo nmroVnfd, VNFDescriptorEx osmVnfd)
	{
		if (osmVnfd.getVduList() == null)
			return;
		
		for (VDU osmVdu : osmVnfd.getVduList())
		{
			VnfPackageSoftwareImageInformation vnfPkgSwImgInf = new VnfPackageSoftwareImageInformation(nmroVnfd, "accessInfo-" + osmVdu.getImage());
			
			//TODO: Cannot assign the SoftwareImageInformation to the VnfPackageSoftwareImageInformation
			SoftwareImageInformation swImgInf = new SoftwareImageInformation(vnfPkgSwImgInf, "ID", osmVdu.getImage(), "provider", "version", osmVdu.getImageChecksum(), "containerFormat", "diskFormat", null, 0, 0, 0, null);
			
			nmroVnfd.getSoftwareImage().add(vnfPkgSwImgInf);
		}
	}
	
	// Map VNFDescriptor into OnboardedVnfPkgInfo
	private OnboardedVnfPkgInfo vnfPkgFromOsmToNmro (VNFDescriptorEx objIn)
	{
		OnboardedVnfPkgInfo vnfdInfo = null;
		
		Vnfd vnfd = vnfdFromOsmToNmro(objIn, vnfdInfo);
		
		//TODO: Complete translation
		List<VnfPackageSoftwareImageInformation> softwareImageList = new ArrayList<VnfPackageSoftwareImageInformation>();
		List<VnfPackageArtifactInformation> additionalArtifactList = new ArrayList<VnfPackageArtifactInformation>();
		
		vnfdInfo = new OnboardedVnfPkgInfo(objIn.getVnfdId(),
				objIn.getId(),
				objIn.getVendor(),
				"vnfProductName - TODO",
				"vnfSoftwareVersion - TODO",
				objIn.getVersion(),
				"checksum - TODO",
				vnfd,
				softwareImageList,
				additionalArtifactList,
				(objIn.getAdminInfo() != null && objIn.getAdminInfo().getOperationalState() != null && objIn.getAdminInfo().getOperationalState().equals("ENABLED")) ? OperationalState.ENABLED:OperationalState.DISABLED,
				(objIn.getAdminInfo() != null && objIn.getAdminInfo().getUsageState() != null && objIn.getAdminInfo().getUsageState().equals("IN_USE")) ? UsageState.IN_USE:UsageState.NOT_IN_USE,
				false,
				null);
		
		softwareImageListFromOsmToNmro (vnfdInfo, objIn);
		
		return vnfdInfo;
	}
	
	public <T1, T2> T1 FromOsmToNmro (T2 objIn)
	{
		if (objIn.getClass() == NSInstance.class)
		{
			return (T1) nsiFromOsmToNmro((NSInstance) objIn);
		}
		/*else if (objIn.getClass() == NSDescriptor.class)
		{
			return (T1) nsdFromOsmToNmro((NSDescriptor) objIn);
		}*/
		else if (objIn.getClass() == VNFDescriptorEx.class)
		{
			return (T1) vnfPkgFromOsmToNmro((VNFDescriptorEx) objIn);
		}
		
		return null;
	}
	
	// Map NSDescriptor into NsdInfo
	private NsdInfo nsdFromOsmToNmro (NSDescriptorEx objIn1, List<VNFDescriptorEx> objIn2)
	{
		// Create onboardedVnfPkgInfoId List
		List<String> onboardedVnfPkgInfoIdList = new ArrayList<String>();
		// TODO: Update from latest version of 5G-Catalog OSM Data Model --> To test!
		for (ConstituentVNFD vnfdRef : objIn1.getConstituentVNFDs())
		{
			for (VNFDescriptorEx vnfdEx : objIn2)
			{
				if (vnfdRef.getVnfdIdentifierReference().equals(vnfdEx.getId()))
					onboardedVnfPkgInfoIdList.add(vnfdEx.getVnfdId());
			}
		}
		
		NsdInfo nsdInfo = new NsdInfo(objIn1.getNsdId(),
				objIn1.getNsdId(),
				objIn1.getName(),
				objIn1.getVersion(),
				objIn1.getVendor(),
				null,
				onboardedVnfPkgInfoIdList,
				null,
				"",
				objIn1.getAdminInfo().getOperationalState().equals("ENABLED") ? OperationalState.ENABLED:OperationalState.DISABLED,
				objIn1.getAdminInfo().getUsageState().equals("IN_USE") ? UsageState.IN_USE:UsageState.NOT_IN_USE,
				false,
				null);
		
		return nsdInfo;
	}
	
	public <T1, T2, T3> T1 FromOsmToNmro (T2 objIn1, T3 objIn2)
	{
		if (objIn1.getClass() == NSDescriptorEx.class)
		{
			return (T1) nsdFromOsmToNmro ((NSDescriptorEx) objIn1, (List<VNFDescriptorEx>) objIn2);
		}
		return null;
	}
}
