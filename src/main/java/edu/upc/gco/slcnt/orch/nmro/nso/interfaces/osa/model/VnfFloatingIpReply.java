package edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model;

public class VnfFloatingIpReply {

	private String vnfVmId;
	private String vimAccountId;
	private String floatingIp;
	
	public VnfFloatingIpReply()
	{
		
	}
	
	public VnfFloatingIpReply (String vnfVmId, String vimAccountId, String floatingIp)
	{
		this.vnfVmId = vnfVmId;
		this.vimAccountId = vimAccountId;
		this.floatingIp = floatingIp;
	}

	public String getVnfVmId() {
		return vnfVmId;
	}

	public String getVimAccountId() {
		return vimAccountId;
	}

	public String getFloatingIp() {
		return floatingIp;
	}
}
