package edu.upc.gco.slcnt.orch.nmro.nso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.core.DCTenant;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmDriver;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmTranslator;
import edu.upc.gco.slcnt.rest.client.osm.model.catalogue.nsDescriptor.NSDescriptorEx;
import edu.upc.gco.slcnt.rest.client.osm.model.catalogue.vnfDescriptor.VNFDescriptorEx;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstance;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.NsdManagementConsumerInterface;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.NsdManagementProviderInterface;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.VnfPackageManagementConsumerInterface;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.VnfPackageManagementProviderInterface;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.elements.NsdInfo;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.DeleteNsdRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.DeleteNsdResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.DeletePnfdRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.DeletePnfdResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.DeleteVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.DisableNsdRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.DisableVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.EnableNsdRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.EnableVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.FetchOnboardedVnfPackageArtifactsRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnBoardVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnBoardVnfPackageResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnboardNsdRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnboardPnfdRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.QueryNsdResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.QueryOnBoardedVnfPkgInfoResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.QueryPnfdResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.UpdateNsdRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.UpdatePnfdRequest;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.AlreadyExistingEntityException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MethodNotImplementedException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.libs.ifa.common.messages.SubscribeRequest;
import it.nextworks.nfvmano.libs.ifa.descriptors.onboardedvnfpackage.OnboardedVnfPkgInfo;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.QueryNsResponse;
import it.nextworks.nfvmano.libs.osmr4PlusClient.utilities.OSMHttpResponse;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.vnfDescriptor.VNFDescriptor;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;
import it.nextworks.nfvmano.libs.ifa.common.enums.ResponseCode;

/**
 * This service manages the external requests for NS catalogue management.
 * It interacts with the internal repositories of NS and VNF descriptors for synchronous 
 * actions and sends messages to the engine for asynchronous actions.
 * 
 * Its methods are invoked by the NmroNsCatalogueManagementController class.
 * 
 * @author UPC based on Nextworks
 *
 */
@Service
public class NmroNsCatalogueService implements NsdManagementProviderInterface, VnfPackageManagementProviderInterface {

	private static final Logger log = LoggerFactory.getLogger(NmroNsCatalogueService.class);
			
	private Nmro nmro;
	private OsmDriver osmDriver;
	private OsmTranslator osmTranslator;
	
	public NmroNsCatalogueService ()
	{
		
	}
	
	public void initService (Nmro nmro)
	{
		this.nmro = nmro;
		this.osmDriver = nmro.getOsmDriver();
		this.osmTranslator = nmro.getOsmTranslator();
	}
	
	// Private aux functions
	
	private List<VNFDescriptorEx> getVnfPackageList ()
	{
		OSMHttpResponse response = osmDriver.getOsmCatalogue().getVnfPackageList();
		String responseContent = response.getContent();
		log.debug("RESPONSE: " + responseContent);
		log.info("CODE: " + response.getCode());
		ObjectMapper mapper = new ObjectMapper();
		List<VNFDescriptorEx> vnfPackageList = null;
		try {
			vnfPackageList = mapper.readValue(responseContent, new TypeReference<List<VNFDescriptorEx>>() {});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return vnfPackageList;
	}
	
	// Public Interfaces
	
	@Override
	public OnBoardVnfPackageResponse onBoardVnfPackage(OnBoardVnfPackageRequest request)
			throws MethodNotImplementedException, AlreadyExistingEntityException, FailedOperationException,
			MalformattedElementException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void enableVnfPackage(EnableVnfPackageRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableVnfPackage(DisableVnfPackageRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteVnfPackage(DeleteVnfPackageRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public QueryOnBoardedVnfPkgInfoResponse queryVnfPackageInfo(GeneralizedQueryRequest request)
			throws MethodNotImplementedException, NotExistingEntityException, MalformattedElementException {
		log.info("queryVnfPackageInfo");
		Filter filter = request.getFilter();
		List<String> attributeSelector = request.getAttributeSelector();
		
		List<OnboardedVnfPkgInfo> queryResult = new ArrayList<>();
		
		if ((attributeSelector == null) || (attributeSelector.isEmpty()))
		{
			// TODO: Fine-tune the Filters
			Map<String, String> fp = filter.getParameters();
			if (fp.size() == 0)
			{
				List<VNFDescriptorEx> vnfdList = getVnfPackageList ();
				
				for (VNFDescriptorEx vnfd : vnfdList)
				{
					queryResult.add(osmTranslator.FromOsmToNmro(vnfd));
				}
			}
			else if (fp.size() == 1 && fp.containsKey("VNFD_ID"))
			{
				OSMHttpResponse response = osmDriver.getOsmCatalogue().getVnfPackageInfo(fp.get("VNFD_ID"));
				String responseContent = response.getContent();
				log.debug("RESPONSE: " + responseContent);
				log.info("CODE: " + response.getCode());
				ObjectMapper mapper = new ObjectMapper();
				VNFDescriptorEx vnfd = null;
				try {
					vnfd = mapper.readValue(responseContent, VNFDescriptorEx.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				queryResult.add(osmTranslator.FromOsmToNmro(vnfd));
			}
			else
			{
				log.error("Filter not supported.");
				return new QueryOnBoardedVnfPkgInfoResponse(null);
			}
		}
		else
		{
			log.error("Attribute selector not supported.");
			return new QueryOnBoardedVnfPkgInfoResponse(null);
		}
		
		return new QueryOnBoardedVnfPkgInfoResponse(queryResult);
	}

	@Override
	public String subscribeVnfPackageInfo(SubscribeRequest request, VnfPackageManagementConsumerInterface consumer)
			throws MethodNotImplementedException, MalformattedElementException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void unsubscribeVnfPackageInfo(String subscriptionId)
			throws MethodNotImplementedException, NotExistingEntityException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public File fetchOnboardedVnfPackage(String onboardedVnfPkgInfoId)
			throws MethodNotImplementedException, NotExistingEntityException, MalformattedElementException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<File> fetchOnboardedVnfPackageArtifacts(FetchOnboardedVnfPackageArtifactsRequest request)
			throws MethodNotImplementedException, NotExistingEntityException, MalformattedElementException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void abortVnfPackageDeletion(String onboardedVnfPkgInfoId) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void queryVnfPackageSubscription(GeneralizedQueryRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String onboardNsd(OnboardNsdRequest request) throws MethodNotImplementedException,
			MalformattedElementException, AlreadyExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void enableNsd(EnableNsdRequest request) throws MethodNotImplementedException, MalformattedElementException,
			NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableNsd(DisableNsdRequest request) throws MethodNotImplementedException,
			MalformattedElementException, NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String updateNsd(UpdateNsdRequest request)
			throws MethodNotImplementedException, MalformattedElementException, AlreadyExistingEntityException,
			NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeleteNsdResponse deleteNsd(DeleteNsdRequest request) throws MethodNotImplementedException,
			MalformattedElementException, NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QueryNsdResponse queryNsd(GeneralizedQueryRequest request) throws MethodNotImplementedException,
			MalformattedElementException, NotExistingEntityException, FailedOperationException {
		
		Filter filter = request.getFilter();
		List<String> attributeSelector = request.getAttributeSelector();
		
		
		List<NsdInfo> queryResult = new ArrayList<>();
		
		if ((attributeSelector == null) || (attributeSelector.isEmpty()))
		{
			Map<String,String> fp = filter.getParameters();
			if (fp.size() == 0)
			{
				OSMHttpResponse response = osmDriver.getOsmCatalogue().getNsdInfoList();
				String responseContent = response.getContent();
				log.debug("RESPONSE: " + responseContent);
				log.info("CODE: " + response.getCode());
				ObjectMapper mapper = new ObjectMapper();
				List<NSDescriptorEx> nsdList = null;
				try {
					nsdList = mapper.readValue(responseContent, new TypeReference<List<NSDescriptorEx>>() {});
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (NSDescriptorEx nsd : nsdList)
				{
					List<VNFDescriptorEx> vnfPackageList = getVnfPackageList();
					
					queryResult.add(osmTranslator.FromOsmToNmro(nsd, vnfPackageList));
				}
			}
			else if ((fp.size() == 1) && (fp.containsKey("NSD_ID")))
			{
				OSMHttpResponse response = osmDriver.getOsmCatalogue().getNsdInfo(fp.get("NSD_ID"));
				
				if (response.getCode() == HttpStatus.NOT_FOUND.value())
					return new QueryNsdResponse(null);
				
				String responseContent = response.getContent();
				log.debug("RESPONSE: " + responseContent);
				log.info("CODE: " + response.getCode());
				ObjectMapper mapper = new ObjectMapper();
				NSDescriptorEx nsd = null;
				try {
					nsd = mapper.readValue(responseContent, NSDescriptorEx.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				List<VNFDescriptorEx> vnfPackageList = getVnfPackageList();
				
				queryResult.add(osmTranslator.FromOsmToNmro(nsd, vnfPackageList));
			}
			// For Slice-O to get the NSDs associated to a DSP Tenant. TENANT_ID == NMR-O Tenant --> id (UUID)
			else if ((fp.size() == 1) && (fp.containsKey("TENANT_ID")))
			{
				DCTenant nmroTenant = (DCTenant)this.nmro.getTenants().get(fp.get("TENANT_ID"));
				if (nmroTenant == null)
				{
					log.error("Tenant " + fp.get("TENANT_ID") + " Not found!");
					return new QueryNsdResponse(null);
				}
				OSMHttpResponse response = nmroTenant.getOsmClient().getOsmCatalogue().getNsdInfoList();
				String responseContent = response.getContent();
				log.debug("RESPONSE: " + responseContent);
				log.info("CODE: " + response.getCode());
				ObjectMapper mapper = new ObjectMapper();
				List<NSDescriptorEx> nsdList = null;
				try {
					nsdList = mapper.readValue(responseContent, new TypeReference<List<NSDescriptorEx>>() {});
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (NSDescriptorEx nsd : nsdList)
				{
					List<VNFDescriptorEx> vnfPackageList = getVnfPackageList();
					
					queryResult.add(osmTranslator.FromOsmToNmro(nsd, vnfPackageList));
				}
			}
			else
			{
				log.error("Filter not supported.");
				return new QueryNsdResponse(null);
			}
		}
		else
		{
			log.error("Attribute selector not supported.");
			return new QueryNsdResponse(null);
		}
		
		return new QueryNsdResponse(queryResult);
	}

	@Override
	public String subscribeNsdInfo(SubscribeRequest request, NsdManagementConsumerInterface consumer)
			throws MethodNotImplementedException, MalformattedElementException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void unsubscribeNsdInfo(String subscriptionId) throws MethodNotImplementedException,
			MalformattedElementException, NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String onboardPnfd(OnboardPnfdRequest request) throws MethodNotImplementedException,
			MalformattedElementException, AlreadyExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updatePnfd(UpdatePnfdRequest request)
			throws MethodNotImplementedException, MalformattedElementException, NotExistingEntityException,
			AlreadyExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeletePnfdResponse deletePnfd(DeletePnfdRequest request) throws MethodNotImplementedException,
			MalformattedElementException, NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QueryPnfdResponse queryPnfd(GeneralizedQueryRequest request) throws MethodNotImplementedException,
			MalformattedElementException, NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

}
