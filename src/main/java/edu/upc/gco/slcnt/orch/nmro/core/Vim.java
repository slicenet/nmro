package edu.upc.gco.slcnt.orch.nmro.core;

import edu.upc.gco.slcnt.orch.nmro.eim.EimDriver;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.OpenStackDriverThread;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.VimInfoObject;

public class Vim {

	private VimInfoObject osmVim;
	private EimDriver eimDriver;
	private DCTenant tenant;
	
	public Vim (VimInfoObject osmVim, DCTenant tenant)
	{
		this.osmVim = osmVim;
		this.tenant = tenant;
		
		init();
	}

	private void init ()
	{
		switch (osmVim.getType())
		{
			case "openstack":
				eimDriver = new OpenStackDriverThread (osmVim.getUrl(), osmVim.getTenantName(), osmVim.getConfig().getDomainName(), osmVim.getUser(), tenant.getOsmClient().getDecrypted(osmVim.getPassword(),osmVim.getId()));
				//ORO
				//eimDriver = new OpenStackDriverThread (osmVim.getUrl(), osmVim.getTenantName(), osmVim.getConfig().getDomainName(), "admin", "b00strap");
				((OpenStackDriverThread) eimDriver).initOpenStackDriver();
				break;
			default:
				break;
		}
	}
	
	public VimInfoObject getOsmVim() {
		return osmVim;
	}

	public EimDriver getEimDriver() {
		return eimDriver;
	}
}
