package edu.upc.gco.slcnt.orch.nmro.core;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import edu.upc.gco.slcnt.orch.nmro.cp.ControlPlaneDriver;
import edu.upc.gco.slcnt.orch.nmro.eim.EimDriver;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.OpenStackDriverThread;
import edu.upc.gco.slcnt.orch.nmro.nso.NmroCsInventoryService;
import edu.upc.gco.slcnt.orch.nmro.nso.NmroCsLifecycleService;
import edu.upc.gco.slcnt.orch.nmro.nso.NmroManagementService;
import edu.upc.gco.slcnt.orch.nmro.nso.NmroNsCatalogueService;
import edu.upc.gco.slcnt.orch.nmro.nso.NmroNsInventoryService;
import edu.upc.gco.slcnt.orch.nmro.nso.NmroNsLifecycleService;
import edu.upc.gco.slcnt.orch.nmro.nso.OSAInventoryService;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmDriver;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmTenantClient;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmTranslator;
import edu.upc.gco.slcnt.orch.nmro.util.AESTest;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.ProjectObject;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.VimInfoObject;

public class Nmro {

	private Logger log = LoggerFactory.getLogger(Nmro.class);
	
	private HashMap<String, Tenant> tenants;		// String Project Id at OSM level => Modified to Tenant Id (UUID)
	
	@Value("${nsp.dc}")
	private String hasDC;
	@Value("${nsp.network}")
	private String hasNetwork;
	private boolean nspHasDC, nspHasNetwork;
	
	@Autowired
	private ControlPlaneDriver cpDriver;
	
	@Autowired
	private OsmDriver osmDriver;
	
	@Autowired
	private OsmTranslator osmTranslator;
	
	@Autowired
	private NmroNsLifecycleService nsLcmService;
	
	@Autowired
	private NmroNsCatalogueService nsCatalogueService;
	
	@Autowired
	private OSAInventoryService osaInventoryService;
	
	@Autowired
	private NmroNsInventoryService nsInventoryService;
	
	@Autowired
	private NmroManagementService mngmtService;
	
	@Autowired
	private NmroCsLifecycleService csLcmService;
	
	@Autowired
	private NmroCsInventoryService csInventoryService;
	
	public OsmDriver getOsmDriver()
	{
		return osmDriver;
	}
	
	public OsmTranslator getOsmTranslator()
	{
		return osmTranslator;
	}
	
	public NmroNsLifecycleService getNsLcmService()
	{
		return nsLcmService;
	}
	
	public OSAInventoryService getOsaInventoryService()
	{
		return osaInventoryService;
	}
	
	public NmroNsInventoryService getNmroNsInventoryService()
	{
		return nsInventoryService;
	}
	
	public NmroManagementService getNmroManagementService()
	{
		return mngmtService;
	}
	
	public NmroCsLifecycleService getNmroCsLifecycleService()
	{
		return csLcmService;
	}
	
	public NmroCsInventoryService getNmroCsInventoryService()
	{
		return csInventoryService;
	}
	
	/*public EimDriver getEimDriver()
	{
		return eimDriver;
	}*/
	
	public ControlPlaneDriver getCpDriver() {
		return cpDriver;
	}

	public Nmro ()
	{
		
	}
	
	public void initializeNmro()
	{
		log.info("Initializing NMR-O");
		
		tenants = new HashMap<String, Tenant>();
		
		// Setting NSP Properties
		if (this.hasDC != null && this.hasDC.equalsIgnoreCase("True"))
			this.nspHasDC = true;
		else
			this.nspHasDC = false;
		
		if (this.hasNetwork != null && this.hasNetwork.equalsIgnoreCase("True"))
			this.nspHasNetwork = true;
		else
			this.nspHasNetwork = false;
		
		if (!this.nspHasDC && this.nspHasNetwork)
		{
			log.info("Initializing CP Driver");
			cpDriver.initializeControlPlaneDriver();
			//Build Tenant instead of Getting it? ==> No underlying VIM
			
			NetworkTenant tenant = new NetworkTenant (1, "Network NSP");
			log.info(tenant.toString());
			tenants.put(tenant.getId().toString(), tenant);
			
			log.info("Initializing Connectivity Service LCM, Inventory and Management Services");
			csLcmService.initService(this);
			csInventoryService.initService(this);
			mngmtService.initService(this);
		}
		else if (this.nspHasDC)
		{
			log.info("Initializing CP Driver");
			cpDriver.initializeControlPlaneDriver();
			
			log.info("Initializing OSM Driver");
			osmDriver.initOsmDriver();
			
			log.info("Initializing Tenants");
			
			ResponseEntity<List<ProjectObject>> getOsmProjects = (ResponseEntity<List<ProjectObject>>) osmDriver.getOsmLcm().getProjects();
			int localId = 1;
			for (ProjectObject po : getOsmProjects.getBody())
			{
				// TODO: Right now, with this condition there will be just one Tenant => Remove {osm.project} from application.properties and keep an association between OSM Projects and DSP Tenants (e.g. One project per tenant)
				if (po.getName().equals(osmDriver.getOsmProject()))
				{
					DCTenant tenant = new DCTenant(localId, po.getName(), po.getId());
					log.info("Creating tenant #" + localId + " / " + po.getName() + " / " + po.getId());
					
					// Create OSM Client for Tenant
					OsmTenantClient osmTenantClient = new OsmTenantClient(osmDriver, osmDriver.getOsmIpAddress(), osmDriver.getOsmPort(), osmDriver.getOsmUser(), osmDriver.getOsmPwd(), po.getName());
					tenant.setOsmClient(osmTenantClient);
					tenant.initalizeTenant();
					
					log.info(tenant.toString());
					//tenants.put(po.getId(), tenant);
					tenants.put(tenant.getId().toString(), tenant);
					localId++;
				}
				else
					continue;
			}
			
			// Create admin tenant (default) --> OSM in NSP1 in Dell testbed has no created Projects, NS are in the "admin" project
			if (tenants.size() == 0)
			{
				DCTenant defaultTenant = new DCTenant(0, "admin", "admin");
				log.info("Creating default tenant #0 / admin / admin");
				OsmTenantClient osmDefaultTenantClient = new OsmTenantClient(osmDriver, osmDriver.getOsmIpAddress(), osmDriver.getOsmPort(), "admin", "admin", "admin");
				defaultTenant.setOsmClient(osmDefaultTenantClient);
				defaultTenant.initalizeTenant();
				log.info(defaultTenant.toString());
				//tenants.put("admin", defaultTenant);
				tenants.put(defaultTenant.getId().toString(), defaultTenant);
			}
			
			//Debug OpenStack client in ORO
			/*log.info ("Number of Tenants = " + tenants.size());
			for (Tenant t : tenants.values())
			{
				for (Vim vim : t.getVimAccounts().values())
				{
					ArrayList<String> vmInfo = ((OpenStackDriverThread)vim.getEimDriver()).getVmInfo("IoT-application-Slice1");
					if (vmInfo != null)
						log.info("VM Info = " + vmInfo.get(0) + " / " + vmInfo.get(1));
					else
						log.info("No VM Info found!");
				}
			}*/
			//End Debug OpenStack client in ORO
			
			log.info("Initializing Network Service LCM, Catalogue, Inventory and Management Services");
			nsLcmService.initService(this);
			nsCatalogueService.initService(this);
			osaInventoryService.initService(this);
			nsInventoryService.initService(this);
			mngmtService.initService(this);
		}
		else if (this.nspHasDC && this.nspHasNetwork)
		{
			// TODO: Assume hybrid NSP
		}
	}
	
	public VimInfoObject selectVimFromTenant (DCTenant tenant)
	{
		// TODO: Logic to select a VIM Account for the Tenant
		log.debug("Select a VIM for Tenant = " + tenant.getName());
				
		//return tenant.getVimAccounts().get("6221b8b2-a737-4a94-87f9-5c1b13e3a6e7").getOsmVim();	//DELL - NSP2
		//return tenant.getVimAccounts().get("bb8f4140-ffee-4270-9ee5-e06f053324ea").getOsmVim();	//DELL - NSP1
		//return (tenant.getVimAccounts().values().size() > 0? tenant.getVimAccounts().values().iterator().next().getOsmVim():null);	//First Fit
		
		if (tenant.getVimAccounts().size() == 1)
			return (tenant.getVimAccounts().values().size() > 0? tenant.getVimAccounts().values().iterator().next().getOsmVim():null);	//First Fit -> ORO/ALB
		else if (tenant.getVimAccounts().size() > 1)
		{
			Vim vim = tenant.getVimAccounts().get("6221b8b2-a737-4a94-87f9-5c1b13e3a6e7");
			if (vim != null)
				return vim.getOsmVim();	//DELL NSP-2
			else
			{
				vim = tenant.getVimAccounts().get("bb8f4140-ffee-4270-9ee5-e06f053324ea");
				if (vim != null)
					return vim.getOsmVim();	//DELL NSP-1
				else
					log.error("No VIM Found!");
			}
		}
		
		return null;
	}
	
	public void createNetworkService (String tenantName, VimInfoObject vim, String nsiId, String nsName, String nsdId)
	{
		log.debug("Create NSI '" + nsiId + "' in Tenant '" + tenantName + "'");
		DCTenant tenant = (DCTenant)tenants.get(tenantName);
		if (tenant == null)
		{
			log.error("Tenant '" + tenantName + "' Not Found!");
			return;
		}
		
		NetworkService ns = new NetworkService(tenant, vim, nsiId, nsName, nsdId);
		tenant.getNetworkServices().put(nsiId, ns);
	}
	
	public NetworkService findNetworkServiceByNsiId (String nsiId)
	{
		for (Tenant tenant : tenants.values())
		{
			NetworkService ns = ((DCTenant)tenant).getNetworkServices().get(nsiId);
			if (ns != null)
				return ns;
		}
		
		return null;
	}
	
	public NetworkService findNetworkServiceByOperationId (String operationId)
	{
		for (Tenant tenant : tenants.values())
		{
			for (NetworkService ns : ((DCTenant)tenant).getNetworkServices().values())
			{
				// Skip the NSs that were not instantiated through the NMR-O (initializeTenants)
				if (ns.getOperationId() == null)
					continue;
				if (ns.getOperationId().equals(operationId))
					return ns;
			}
		}
		
		return null;
	}
	
	public ConnectivityService findConnectivityServiceByCsiId (String csiId)
	{
		for (Tenant tenant : tenants.values())
		{
			ConnectivityService cs = ((NetworkTenant)tenant).getConnectivityServices().get(csiId);
			if (cs != null)
				return cs;
		}
		
		return null;
	}
	
	public void deleteNetworkService (NetworkService ns)
	{
		String nsiId = ns.getNsiId();
		DCTenant tenant = ns.getTenant();
		boolean removed = tenant.getNetworkServices().remove(nsiId, ns);
		if (removed)
		{
			log.debug("Removed Network Service Instance '" + nsiId + "' from Tenant '" + tenant.getName() + "'");
			return;
		}
		else
			log.error("Could not remove Network Service Instance '" + nsiId + "' from Tenant '" + tenant.getName() + "'");
	}
	
	public void deleteConnectivityService (ConnectivityService cs)
	{
		String csiId = cs.getCsiId();
		NetworkTenant tenant = cs.getTenant();
		boolean removed = tenant.getConnectivityServices().remove(csiId, cs);
		if (removed)
		{
			log.debug("Removed Connectivity Service Instance '" + csiId + "' from Tenant '" + tenant.getName() + "'");
			return;
		}
		else
			log.error("Could not remove Connectivity Service Instance '" + csiId + "' from Tenant '" + tenant.getName() + "'");
	}
	
	public DCTenant getTenantByProjectId (String projectId)
	{
		for (Tenant t : tenants.values())
		{
			if (((DCTenant)t).getProjectId().equals(projectId))
				return (DCTenant)t;
		}
		
		return null;
	}

	public HashMap<String, Tenant> getTenants() {
		return tenants;
	}
	
	public boolean isNspHasDC() {
		return nspHasDC;
	}

	public boolean isNspHasNetwork() {
		return nspHasNetwork;
	}
}
