package edu.upc.gco.slcnt.orch.nmro.nso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.upc.gco.slcnt.rest.client.nmro.interfaces.mngmt.messages.GetTenantsResponse;

@RestController
@RequestMapping("/nmro/mngmt")
public class NmroManagementController {

	private static final Logger log = LoggerFactory.getLogger(NmroManagementController.class);
	
	@Autowired
	private NmroManagementService mngmtService;
	
	public NmroManagementController() { }
	
	@RequestMapping(value = "/tenants", method = RequestMethod.GET)
	public GetTenantsResponse getTenants ()
	{
		log.debug("Received Get Tenants Request");
		GetTenantsResponse response = mngmtService.getTenants();
		
		return response;
	}
}
