package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nfvo/inventory")
public class NmroNsInventoryController {

	private static final Logger log = LoggerFactory.getLogger(NmroNsInventoryController.class);
	
	@Autowired
	private NmroNsInventoryService inventoryService;
	
	@RequestMapping(value = "/nsi/{nsiId}", method = RequestMethod.GET)
	public List<String> getNsiResources (@PathVariable String nsiId)
	{
		return inventoryService.getNsiResources(nsiId);
	}
	
	@RequestMapping(value = "/nsi/{nsiId}/clients", method = RequestMethod.GET)
	public List<String> getNsiClients (@PathVariable String nsiId)
	{
		return inventoryService.getNsiClients(nsiId);
	}
}
