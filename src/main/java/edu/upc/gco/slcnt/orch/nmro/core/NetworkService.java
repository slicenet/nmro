package edu.upc.gco.slcnt.orch.nmro.core;

import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.VimInfoObject;

public class NetworkService {

	private DCTenant tenant;
	private VimInfoObject vim;
	private String nsiId;		// NSI Id at OSM (NMR-O) level
	
	// Needed for instantiation
	private String nsName;
	private String nsdId;
	private String operationId;	// Operation Occurrence -> Needed to check the operational status of the NSI (Not implemented in OSM)
	
	public NetworkService ()
	{
		
	}
	
	public NetworkService (DCTenant tenant, VimInfoObject vim, String nsiId, String nsName, String nsdId)
	{
		this.tenant = tenant;
		this.vim = vim;
		this.nsiId = nsiId;
		this.nsName = nsName;
		this.nsdId = nsdId;
	}

	public DCTenant getTenant() {
		return tenant;
	}

	public void setTenant(DCTenant tenant) {
		this.tenant = tenant;
	}

	public VimInfoObject getVim() {
		return vim;
	}

	public void setVim(VimInfoObject vim) {
		this.vim = vim;
	}

	public String getNsiId() {
		return nsiId;
	}

	public void setNsiId(String nsiId) {
		this.nsiId = nsiId;
	}

	public String getNsName() {
		return nsName;
	}

	public void setNsName(String nsName) {
		this.nsName = nsName;
	}

	public String getNsdId() {
		return nsdId;
	}

	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	@Override
	public String toString() {
		return "NetworkService [tenant=" + tenant.getName() + ", vim=" + vim.getName() + ", nsiId=" + nsiId + "]";
	}
}
