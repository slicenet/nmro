package edu.upc.gco.slcnt.orch.nmro.core;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import edu.upc.gco.slcnt.orch.nmro.osm.OsmTenantClient;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstance;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.VimInfoObject;

public class DCTenant extends Tenant {

	private Logger log = LoggerFactory.getLogger(DCTenant.class);
	
	private String projectId;									// OSM Project Id
	//private HashMap<String, VimInfoObject> vimAccounts;		// String is VIM account Id at NFV-O
	private HashMap<String, Vim> vimAccounts;					// String is VIM account Id at NFV-O
	private HashMap<String, NetworkService> networkServices;	// String is the nsiId at the NFV-O
	private OsmTenantClient osmClient;
	
	public DCTenant()
	{
		
	}
	
	public DCTenant (int localId, String name, String projectId)
	{
		this.localId = localId;
		this.id = UUID.randomUUID();
		this.name = name;
		this.projectId = projectId;
		//this.vimAccounts = new HashMap<String, VimInfoObject>();
		this.vimAccounts = new HashMap<String, Vim>();
		this.networkServices = new HashMap<String, NetworkService>();
	}
	
	public void initalizeTenant ()
	{
		// Get VIM List associated to the Tenant (ProjectId)
		ResponseEntity<List<VimInfoObject>> reGetVims = (ResponseEntity<List<VimInfoObject>>) osmClient.getOsmLcm().getVimInfoList();
		
		// Get VIM accounts for the Tenant
		for (VimInfoObject vio : reGetVims.getBody())
		{
			// First VIM in NSP1 of Dell is not reachable - I skip it
			if (vio.getName().equals("vio"))
				continue;
			// Skip OpenStack108 in Dell NSP2 => Not reachable
			if (vio.getName().equals("OpenStack108"))
				continue;
			
			// I think this check should not be necessary since the Token is associated to the Project
			for (String project : vio.getAdmin().getProjectsWrite())
			{
				if (project.equals(projectId))
				{
					log.info("Adding VIM = " + vio.getId());
					//vimAccounts.put(vio.getId(), vio);
					Vim vim = new Vim(vio,this);
					vimAccounts.put(vio.getId(), vim);
				}
			}
		}
		
		// Get existing NS instances of the Tenant
		ResponseEntity<List<NSInstance>> reGetNSIs = (ResponseEntity<List<NSInstance>>) osmClient.getOsmLcm().getNsiInfoList();
		for (NSInstance nsi : reGetNSIs.getBody())
		{
			// I think this check should not be necessary since the Token is associated to the Project
			for (String project : nsi.getAdminObject().getProjectsWrite())
			{
				if (project.equals(projectId))
				{
					String nsiId = nsi.getId();
					log.info("Adding NSI = " + nsiId);
					VimInfoObject vimAccount = null;
					for (VimInfoObject vio : reGetVims.getBody())
					{
						log.info("Adding VIM = " + vio.getId());
						if (vio.getId().equals(nsi.getDatacenterId()))
						{
							log.info("Added!");
							vimAccount = vio;
						}
					}
					NetworkService ns = new NetworkService (this, vimAccount, nsiId, nsi.getName(), nsi.getNsdId());
					networkServices.put(nsiId, ns);
				}
			}
		}
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/*public HashMap<String, VimInfoObject> getVimAccounts() {
		return vimAccounts;
	}

	public void setVimAccounts(HashMap<String, VimInfoObject> vimAccounts) {
		this.vimAccounts = vimAccounts;
	}*/
	public HashMap<String, Vim> getVimAccounts() {
		return vimAccounts;
	}

	public void setVimAccounts(HashMap<String, Vim> vimAccounts) {
		this.vimAccounts = vimAccounts;
	}

	public HashMap<String, NetworkService> getNetworkServices() {
		return networkServices;
	}

	public void setNetworkServices(HashMap<String, NetworkService> networkServices) {
		this.networkServices = networkServices;
	}

	public OsmTenantClient getOsmClient() {
		return osmClient;
	}

	public void setOsmClient(OsmTenantClient osmClient) {
		this.osmClient = osmClient;
	}

	@Override
	public String toString() {
		return "Tenant [id=" + id + ", name=" + name + ", projectId=" + projectId + ", networkServices=" 
				+ ((networkServices.size()==0)?"empty":networkServices) + "]";
	}
}
