package edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.HostResource;
import org.openstack4j.model.compute.HostResourceBody;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.compute.ext.Hypervisor;
import org.openstack4j.model.identity.v3.Token;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpRescaleVM implements Callable<Boolean> {

	private Logger log = LoggerFactory.getLogger(OpRescaleVM.class);
	private Token token;
	private String vmId;
	private int cpuScale;
	private int memoryScale;
	private int storageScale;
	
	/* The fields cpuScale, memoryScale, storageScale are used to indicate what is going to be scaled at the VM with the values
	 * indicating the exact operation: 0 = no scaling, 1 = scale-up, 2 = scale-down. These fields would be part of the key/value
	 * pairs at the Actuation Request*/
	
	public OpRescaleVM (Token token, String vmId, int cpuScale, int memoryScale, int storageScale)
	{
		this.token = token;
		this.vmId = vmId;
		this.cpuScale = cpuScale;
		this.memoryScale = memoryScale;
		this.storageScale = storageScale;
	}

	@Override
	public Boolean call() throws Exception {
		
		if(cpuScale == 0 && memoryScale == 0 && storageScale == 0) {
			log.info("No rescaling operation performed");
			return true;
		}
		else {
			
			OSClientV3 osClient = OSFactory.clientFromToken(token);
			
			Flavor flavor = osClient.compute().servers().get(vmId).getFlavor();
	    	
	    	int cpus = flavor.getVcpus();
	    	
	    	int memory = flavor.getRam();
	    	
	    	int storage = flavor.getDisk();
	    	
	    	int ephemeral = flavor.getEphemeral();
			
	    	int swap = flavor.getSwap();
	    	
	    	float rxtxFactor = flavor.getRxtxFactor();
	    	
	    	boolean isPublic = flavor.isPublic();
	    	
			int new_cpus;
			int new_memory;
			int new_storage;
			
			if (cpuScale == 0) {
				
				new_cpus = cpus;
			}
			else if(cpuScale == 1) {
				
				new_cpus = cpus + 1;
			}
			else {
				
				new_cpus = cpus - 1;
			}
					
			if(memoryScale == 0) {
				
				new_memory = memory;
			}
			else if(memoryScale == 1) {
				
				new_memory = memory + 64;
			}
			else {
				
				new_memory = memory - 64;
			}
					
			if(storageScale == 0) {
				
				new_storage = storage;
			}
			else if(storageScale == 1) {
				
				new_storage = storage + 1;
			}
			else {
				
				new_storage = storage - 1;
			}
			
			String new_flavorId = ""; //Id of the flavor employed for scaling, either an existing one or a new one created in purpose
			
			if(new_cpus<= 0 && new_memory <=0 && new_storage <=0)
			{
				log.info("No rescaling operation performed");
			return true;
			}

			String hostId = osClient.compute().servers().get(vmId).getHypervisorHostname();
			
			List<? extends Hypervisor> hosts = osClient.compute().hypervisors().list();
			
			Iterator<? extends Hypervisor> itH = hosts.iterator();
			
			int host_cpus = 0;
			int host_memory = 0;
			int host_storage = 0;
			
			while(itH.hasNext()) {
				
				Hypervisor host = itH.next();
				
				if(host.getHypervisorHostname().equals(hostId)) {
					
					host_cpus = host.getVirtualCPU();
					
					host_memory = host.getLocalMemory();
					
					host_storage = host.getLocalDisk();
				}
			}
			
			log.info("Host resources are: CPUS = " + host_cpus+"; Memory = " + host_memory+"; Storage = " + host_storage);
			
			if(new_cpus>host_cpus && new_memory>host_memory && new_storage>host_storage) {
				log.info("No rescaling operation performed");
				return true;
			}
			
			if(new_cpus>host_cpus || new_cpus<= 0) {
				
				new_cpus = cpus;
			}
			
			if(new_memory>host_memory || new_memory <=0) {
				
				new_memory = memory;
			}
			
			if(new_storage>host_storage || new_storage <=0) {
				
				new_storage = storage;
			}
			
			boolean found = false;
			
			List<? extends Flavor> flavors = osClient.compute().flavors().list();
			
			Iterator<? extends Flavor> it = flavors.iterator();
			
			while(it.hasNext() && !found) {
				
				Flavor f = it.next();
							
				if(f.getVcpus() == new_cpus && f.getDisk() == new_storage && f.getRam() == new_memory) {
					
					new_flavorId = f.getId();
					
					found = true;
				}
			}
			
			if(!found) {
				
				int leftLimit = 97;
			    int rightLimit = 122;
			    int targetStringLength = 10;
			    Random random = new Random();

			    String randomString = random.ints(leftLimit, rightLimit + 1)
			    		.limit(targetStringLength)
			    		.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
			    		.toString();
				
				String name = "m1."+randomString;
				
				Flavor new_flavor = osClient.compute().flavors().create(name, new_memory, new_cpus, new_storage, ephemeral, swap, rxtxFactor, isPublic);
				
				new_flavorId = new_flavor.getId();
			}
			
			ActionResponse response = osClient.compute().servers().resize(vmId, new_flavorId);
			
			if(!response.isSuccess()) {
				log.info("No rescaling operation performed");
				return true;
			}
			else {
				log.info("Rescale operation code = " + response.getCode());
			}
			
			osClient.compute().servers().waitForServerStatus(vmId, Server.Status.VERIFY_RESIZE, 60, TimeUnit.MINUTES);
			
			response = osClient.compute().servers().confirmResize(vmId);
			
			if (!response.isSuccess())
				log.error("Error rescaling (" + response.getCode() + "): " + response.getFault());
			
			return response.isSuccess();
		}
	}
}
