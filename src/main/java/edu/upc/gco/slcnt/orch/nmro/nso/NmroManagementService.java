package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.core.Tenant;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.mngmt.messages.GetTenantsResponse;

@Service
public class NmroManagementService {

	private static final Logger log = LoggerFactory.getLogger(NmroManagementService.class);
	
	private Nmro nmro;
	
	public NmroManagementService () {}
	
	public void initService (Nmro nmro)
	{
		this.nmro = nmro;
	}
	
	public GetTenantsResponse getTenants()
	{
		List<String> tenantIds = new ArrayList<String>();
		
		for (Tenant t : nmro.getTenants().values())
		{
			tenantIds.add(t.getId().toString());
		}
		
		return new GetTenantsResponse(tenantIds);
	}
}
