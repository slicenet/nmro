package edu.upc.gco.slcnt.orch.nmro.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AES {

	private final Logger log = LoggerFactory.getLogger(AES.class);
	
	private String commonKey;
	private String serial;
	
	private byte[] computedSalt;
	private byte[] secret;
	private SecretKeySpec secretKey;
	
	public AES ()
	{
		
	}
	
	public AES (String commonKey, String serial)
	{
		this.commonKey = commonKey;
		this.serial = serial;
		
		computedSalt = joinCommonKey (this.commonKey);
		
		String b64Serial = serialToBase64String (this.serial);
		secret = joinSecretKey (Base64.getDecoder().decode(b64Serial), computedSalt);
	}
	
	private byte[] joinCommonKey (String commonKey)
	{
		byte[] newSecretKeyBytes = new byte[32];
		byte[] commonKeyBytes = commonKey.getBytes();//Base64.getDecoder().decode(commonKey);
		
		for (int i=0; i<commonKeyBytes.length; i++)
			newSecretKeyBytes[i%32] ^= commonKeyBytes[i];
		
		return newSecretKeyBytes;
	}
	
	private String serialToBase64String (String serial)
	{
		byte[] bSerial = Base64.getDecoder().decode(serial);
		String serialStr = "";
		/*for (byte b : bSerial)
			serialStr += String.format("%c", b);*/
		serialStr = new String(bSerial);
		
		return serialStr;
	}
	
	private byte[] joinSecretKey (byte[] updateKey, byte[] secretKey)
	{
		byte[] newSecretKey = null;
		
		if (updateKey == null)
			return secretKey;
		
		if (secretKey == null)
			newSecretKey = new byte[32];
		else
			newSecretKey = Arrays.copyOf(secretKey, /*secretKey.length*/32);
		
		//log.info("[joinSecretKey] length = " + updateKey.length);
		for (int i=0; i < updateKey.length; i++)
		{
			//log.info("[joinSecretKey] i = " + i + ", newSecretKey[i%32] = " + newSecretKey[i%32] + ", updateKey[i] = " + updateKey[i]);
			newSecretKey[i%32] ^= updateKey[i];
		}
		return newSecretKey;
	}
	
	private void setKey (byte[] srcKey)
	{
		byte[] key = Arrays.copyOf(srcKey, 32);
		secretKey = new SecretKeySpec(key, "AES");
	}
	
	private String manualZeroPadding (String inputStr)
	{
		String outputStr = inputStr;
		
		int size = 16 - inputStr.length();
		for (; size > 0; size--)
			outputStr += '\0';
		
		return outputStr;
	}
	
	public String encrypt (String strToEncrypt, String salt)
	{
		byte[] saltBytes = salt.getBytes();
		
		byte[] newSecretKeyBytes = joinSecretKey(saltBytes, secret);
		
		setKey(newSecretKeyBytes);
		
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			String strToEncryptPadding = manualZeroPadding(strToEncrypt);
			
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncryptPadding.getBytes()));
		
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String decrypt (String strToDecrypt, String salt)
	{
		//log.info("Encrypted Password = " + strToDecrypt);
		byte[] saltBytes = salt.getBytes();
		byte[] newSecretKeyBytes = joinSecretKey(saltBytes, secret);
		
		setKey(newSecretKeyBytes);
		
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			String decryptedStr = new String (cipher.doFinal(Base64.getDecoder().decode(strToDecrypt))).trim();//stripTrailing();
			//log.info("Decrypted Password = " + decryptedStr);
			return decryptedStr;
		
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
