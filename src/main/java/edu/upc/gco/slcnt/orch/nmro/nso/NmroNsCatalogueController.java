package edu.upc.gco.slcnt.orch.nmro.nso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.QueryNsdResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.QueryOnBoardedVnfPkgInfoResponse;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MethodNotImplementedException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;



/**
 * This class implements the REST APIs for the NS catalog
 * management service of the NFVO. 
 * The internal implementation of the service is implemented through
 * the NmroNsCatalogueService.
 * 
 * @author UPC based on Nextworks
 *
 */
@RestController
@RequestMapping("/nfvo")
public class NmroNsCatalogueController {

	private static final Logger log = LoggerFactory.getLogger(NmroNsCatalogueController.class);
	
	@Autowired
	NmroNsCatalogueService nsCatalogueService;
	
	public NmroNsCatalogueController() { }
	
	@RequestMapping(value = "/nsdManagement/nsd/query", method = RequestMethod.POST)
	public ResponseEntity<?> queryNsd(@RequestBody GeneralizedQueryRequest request) {
		log.debug("Received query NSD request");
		log.info("Request Filter = " + request.getFilter().getParameters().size());
		try {
			request.isValid();
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		try {
			QueryNsdResponse response = nsCatalogueService.queryNsd(request);
			return new ResponseEntity<QueryNsdResponse>(response, HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("Not existing entities: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (MethodNotImplementedException e) {
			log.error("Method not implemented. " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (FailedOperationException e) {
			log.error("Operation failed: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value = "/vnfdManagement/vnfPackage/query", method = RequestMethod.POST)
	public ResponseEntity<?> queryVnfPackage(@RequestBody GeneralizedQueryRequest request) {
		log.info("Received query VNF package request");
		try {
			request.isValid();
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		try {
			QueryOnBoardedVnfPkgInfoResponse response = nsCatalogueService.queryVnfPackageInfo(request);
			return new ResponseEntity<QueryOnBoardedVnfPkgInfoResponse>(response, HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (MethodNotImplementedException e) {
			log.error("Method not implemented. " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			log.error("Not existing entities: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}
}
