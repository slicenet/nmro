package edu.upc.gco.slcnt.orch.nmro.eim.openstack;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.core.transport.Config;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import edu.upc.gco.slcnt.orch.nmro.eim.EimDriver;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations.OpGetFloatingIP;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations.OpGetValidHostForVm;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations.OpGetVmInfo;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations.OpMigrateVM;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations.OpRescaleVM;
import edu.upc.gco.slcnt.orch.nmro.util.AESTest;

public class OpenStackDriverThread extends EimDriver {

	private Logger log = LoggerFactory.getLogger(OpenStackDriverThread.class);
	
	private String keystoneUrl;
	private String tenantName;
	private String domainName;
	private String userName;
	private String userPwd;
	private OSClientV3 osClient;
	private ExecutorService manager;
	
	public OpenStackDriverThread(String keystoneUrl, String tenantName, String domainName, String userName, String userPwd)
	{
		this.keystoneUrl = keystoneUrl;
		this.tenantName = tenantName;
		//ORO
		if (domainName == null)
			this.domainName = "Default";
		else
			this.domainName = domainName;
		this.userName = userName;
		this.userPwd = userPwd;
		
		manager = Executors.newCachedThreadPool();
	}
	
	public void initOpenStackDriver ()
	{
		log.debug("OpenStack Driver Authenticating at " + keystoneUrl + " With Credentials = " + tenantName + "/" + domainName + "/" + userName + "/"+ userPwd + " I will set domain = Default");
		
		if (keystoneUrl.contains("http:"))
			this.osClient = OSFactory.builderV3()
	                .endpoint(keystoneUrl)
	                .credentials(this.userName, this.userPwd,Identifier.byName(this.domainName))
	                .scopeToProject(Identifier.byName(this.tenantName), Identifier.byName(this.domainName))
	                .authenticate();
		else if (keystoneUrl.contains("https:"))
		{
			// Disable SSL Cert verification
			Config config = Config.newConfig().withSSLVerificationDisabled();
			this.osClient = OSFactory.builderV3()
	                .withConfig(config)
	                .endpoint(keystoneUrl)
	                .credentials(this.userName, this.userPwd,Identifier.byName(this.domainName))
	                .scopeToProject(Identifier.byName(this.tenantName), Identifier.byName(this.domainName))
	                .authenticate();
		}
	}

	public OSClientV3 getOsClient() {
		return osClient;
	}
	
	public boolean rescaleVM (String vmId, int cpuScale, int memoryScale, int storageScale)
	{
		Future<Boolean> result = manager.submit(new OpRescaleVM(osClient.getToken(), vmId, cpuScale, memoryScale, storageScale));
		
		boolean rescaled = false;
		try {
			rescaled = result.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rescaled;
	}
	
	public boolean migrateVM (String vmId, String hostId)
	{
		Future<Boolean> result = manager.submit(new OpMigrateVM(osClient.getToken(), vmId, hostId));
		
		boolean migrated = false;
		try {
			migrated = result.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return migrated;
	}
	
	public String getFloatingIP (String vmId)
	{
		Future<String> result = manager.submit(new OpGetFloatingIP(osClient.getToken(), vmId));
		
		String floatingIp =  null;
		try {
			floatingIp = result.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.warn(" Exception = " + e.getMessage());
		}
		
		return floatingIp;
	}
	
	public String getValidHost (String vmName)
	{
		Future<String> result = manager.submit(new OpGetValidHostForVm(osClient.getToken(), vmName));
		
		String hostName = null;
		try {
			hostName = result.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return hostName;
	}
	
	public ArrayList<String> getVmInfo (String vmName)
	{
		Future<ArrayList<String>> result = manager.submit(new OpGetVmInfo(osClient.getToken(), vmName));
		
		ArrayList<String> vmInfo =  null;
		try {
			vmInfo = result.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return vmInfo;
	}
	
	public void stop ()
	{
		manager.shutdown();
		try {
			if (!manager.awaitTermination(1000, TimeUnit.MILLISECONDS))
				manager.shutdownNow();
		} catch (InterruptedException e) {
			manager.shutdownNow();
		}
	}
}
