package edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations;

import java.util.concurrent.Callable;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.compute.actions.LiveMigrateOptions;
import org.openstack4j.model.identity.v3.Token;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpMigrateVM implements Callable<Boolean> {

	private Logger log = LoggerFactory.getLogger(OpMigrateVM.class);
	private Token token;
	private String vmId;
	private String hostId;
	
	public OpMigrateVM (Token token, String vmId, String hostId)
	{
		this.token = token;
		this.vmId = vmId;
		this.hostId = hostId;
	}

	@Override
	public Boolean call() throws Exception {
		
		OSClientV3 osClient = OSFactory.clientFromToken(token);
		LiveMigrateOptions options = LiveMigrateOptions.create()
				.blockMigration(false)
				.diskOverCommit(false)
				.host(hostId);

		ActionResponse response = osClient.compute().servers().liveMigrate(vmId, options);

		if(!response.isSuccess())
			log.error("Error migrating (" + response.getCode() + "): " + response.getFault());

		return response.isSuccess();
	}
}
