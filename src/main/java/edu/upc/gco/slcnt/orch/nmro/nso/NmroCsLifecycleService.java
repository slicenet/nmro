package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import edu.upc.gco.slcnt.orch.nmro.core.ConnectivityService;
import edu.upc.gco.slcnt.orch.nmro.core.NetworkTenant;
import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.core.Tenant;
import edu.upc.gco.slcnt.orch.nmro.cp.ControlPlaneDriver;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.common.elements.ActuationData;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.common.messages.ActuateServiceRequest;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.elements.CsInfo;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages.CreateCsIdentifierRequest;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages.InstantiateCsRequest;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages.QueryCsResponse;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;
import it.nextworks.nfvmano.libs.ifa.common.enums.ResponseCode;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MethodNotImplementedException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;

@Service
public class NmroCsLifecycleService {

	private Logger log = LoggerFactory.getLogger(NmroCsLifecycleService.class);
	
	private Nmro nmro;
	private ControlPlaneDriver cpDriver;
	
	public NmroCsLifecycleService() { }
	
	public void initService (Nmro nmro)
	{
		this.nmro = nmro;
		this.cpDriver = nmro.getCpDriver();
	}
	
	public String createCsIdentifier (CreateCsIdentifierRequest request) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException
	{
		log.info("Creating CS Identifier");
		String csName = request.getCsName();
		String csDescription = request.getCsDescription();
		log.info("Looking for Tenant = " + request.getTenantId());
		NetworkTenant tenant = (NetworkTenant)nmro.getTenants().get(request.getTenantId());
		
		if (tenant == null)
			throw new NotExistingEntityException("Tenant " + request.getTenantId() + " Not Found");
		log.info("Tenant Found");
		ConnectivityService cs = new ConnectivityService (csName, csDescription, tenant);
		tenant.getConnectivityServices().put(cs.getCsiId(), cs);
		
		return cs.getCsiId();
	}
	
	public String instantiateCs (InstantiateCsRequest request) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException
	{
		String csiId = request.getCsInstanceId();
		ConnectivityService cs = nmro.findConnectivityServiceByCsiId(csiId);
		if (cs == null)
		{
			log.error("Error trying to instantiate CS Instance: Not Found.");
			throw new NotExistingEntityException("CS Instance not found");
		}
		
		log.info("Building FG for " + request.getEndPoints().size() + " End Points");
		String fgStr = cs.buildForwardingGraph(request.getEndPoints());
		log.info("FG = " + fgStr);
		
		// Translate Request Parameters into CS Parameters
		HashMap<String,String> csParameters = new HashMap<String,String>();
		csParameters.put("latency", request.getParameters().get("latency"));
		csParameters.put("bitrate", request.getParameters().get("bitrate"));
		
		String availability = request.getParameters().get("availability");
		if (availability.equals("HIGH"))
		{
			csParameters.put("CoS", "PREMIUM");
			csParameters.put("protection", "True");
		}
		else if (availability.equals("MEDIUM"))
		{
			csParameters.put("CoS", "BESTEFFORT");
			csParameters.put("protection", "True");
		}
		else
		{
			csParameters.put("CoS", "BESTEFFORT");
			csParameters.put("protection", "False");
		}
		
		cs.setParameters(csParameters);
		
		List<String> ipopConnectionIds = nmro.getCpDriver().createInterPoPConnection(cs.getForwardingGraph(), cs.getParameters());
		cs.setIpopConnectionIds(ipopConnectionIds);
		
		return cs.getCsiId();
	}
	
	public QueryCsResponse queryCs (GeneralizedQueryRequest request) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException
	{
		Filter filter = request.getFilter();
		List<String> attributeSelector = request.getAttributeSelector();
		
		List<CsInfo> queryCsResult = new ArrayList<>();
		
		if ((attributeSelector == null) || (attributeSelector.isEmpty()))
		{
			Map<String,String> fp = filter.getParameters();
			if (fp.size() == 0)
			{
				for (Tenant tenant : nmro.getTenants().values())
				{
					if (tenant.getClass() == NetworkTenant.class)
					{
						for (ConnectivityService cs : ((NetworkTenant)tenant).getConnectivityServices().values())
						{
							CsInfo csInfo = new CsInfo(cs.getCsiId(), cs.getName(), cs.getDescription(), cs.getIpopConnectionIds());
							queryCsResult.add(csInfo);
						}
					}
				}
			}
			else if ((fp.size() == 1) && (fp.containsKey("CS_ID")))
			{
				ConnectivityService cs = nmro.findConnectivityServiceByCsiId(fp.get("CS_ID"));
				if (cs == null)
					return new QueryCsResponse(ResponseCode.FAILED_NOT_EXISTING_RESOURCE, null);
				
				CsInfo csInfo = new CsInfo(cs.getCsiId(), cs.getName(), cs.getDescription(), cs.getIpopConnectionIds());
				queryCsResult.add(csInfo);
			}
		}
		else
		{
			log.error("Attribute selector not supported.");
			return new QueryCsResponse(ResponseCode.FAILED_GENERIC, null);
		}
		
		return new QueryCsResponse(ResponseCode.OK, queryCsResult);
	}
	
	public /*ActuationData*/String actuateCs (ActuateServiceRequest request) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException
	{
		//ActuationData actuationData = null;
		String actuationData = null;
		
		String csiId = request.getServiceId();
		
		ConnectivityService cs = nmro.findConnectivityServiceByCsiId(csiId);
		if (cs == null)
		{
			log.error ("CS Instance not found");
			throw new NotExistingEntityException("CS Instance not found");
		}
		
		if (request.getActuationName().equals("Reroute"))
		{
			//actuationData = new ActuationData();
			actuationData = "SUCCESS";
			String iPoPCId = request.getParameters().get("resourceId");
			nmro.getCpDriver().actuateInterPoPConnection(iPoPCId);
		}
		
		return actuationData;
	}
	
	public String terminateCs (String csiId) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException
	{
		ConnectivityService cs = nmro.findConnectivityServiceByCsiId(csiId);
		if (cs == null)
		{
			log.error("Error trying to terminate CS Instance: Not Found.");
			throw new NotExistingEntityException("CS Instance not found");
		}
		
		List<String> resultList = new ArrayList<String>();
		
		for (String iPoPCId : cs.getIpopConnectionIds())
		{
			String result = nmro.getCpDriver().deleteInterPoPConnection(iPoPCId);
			log.info("Deletion result for Inter PoP Connection " + iPoPCId + " is " + result);
			resultList.add(result);
		}
		
		// TODO: Some control on the result (ALL == SUCCESS or something like that), rollback?
		cs.getIpopConnectionIds().clear();
		
		//nmro.deleteConnectivityService(cs);
		
		return csiId;
	}
	
	public String deleteCsIdentifier (String csiId) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException
	{
		ConnectivityService cs = nmro.findConnectivityServiceByCsiId(csiId);
		if (cs == null)
		{
			log.error("Error trying to delete CS Instance Identifier: Not Found.");
			throw new NotExistingEntityException("CS Instance not found");
		}
		
		nmro.deleteConnectivityService(cs);
		
		return csiId;
	}
}
