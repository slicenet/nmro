package edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.messages;

import java.util.List;

import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model.VnfFloatingIpRequest;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

public class FloatingIpRequest implements InterfaceMessage {

	private List<VnfFloatingIpRequest> vnfList;
	
	public FloatingIpRequest()
	{
		
	}
	
	public FloatingIpRequest (List<VnfFloatingIpRequest> vnfList)
	{
		this.vnfList = vnfList;
	}
	
	public List<VnfFloatingIpRequest> getVnfList() {
		return vnfList;
	}

	public void setVnfList(List<VnfFloatingIpRequest> vnfList) {
		this.vnfList = vnfList;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (vnfList == null) throw new MalformattedElementException("Get Floating IPs from an empty list");
	}
}
