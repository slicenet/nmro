package edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model;

public class VnfFloatingIpRequest {

	private String vnfVmId;
	private String vimAccountId;
	
	public VnfFloatingIpRequest (String vnfVmId, String vimAccountId)
	{
		this.vnfVmId = vnfVmId;
		this.vimAccountId = vimAccountId;
	}
	
	public String getVnfVmId() {
		return vnfVmId;
	}
	public String getVimAccountId() {
		return vimAccountId;
	}
}
