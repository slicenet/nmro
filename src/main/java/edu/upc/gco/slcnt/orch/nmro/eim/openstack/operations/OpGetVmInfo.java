package edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.identity.v3.Token;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpGetVmInfo implements Callable<ArrayList<String>> {

	private Logger log = LoggerFactory.getLogger(OpGetVmInfo.class);
	private Token token;
	private String vmName;
	
	public OpGetVmInfo (Token token, String vmName)
	{
		this.token = token;
		this.vmName = vmName;
	}
	
	@Override
	public ArrayList<String> call() throws Exception {
		log.info("Getting OS client");
		OSClientV3 osClient = OSFactory.clientFromToken(token);
		
		ArrayList<String> vmInfo = new ArrayList<String>(0);
		log.info("Getting VMs (Server list)");
		List<? extends Server> VMs = osClient.compute().servers().list(); // OpenStack VMs for Tenant
		Iterator<? extends Server> it = VMs.iterator();
		log.info("Iterate VMs");
		String name = "";
		while(it.hasNext() && !name.equals(vmName)) {
			
			Server vm = it.next();
			
			name = vm.getName();
			log.info("Treating VM = " + name);
			if(name.equals(vmName))
			{
				String id = vm.getId();
				log.info("Adding ID = " + id);
				vmInfo.add(id);
				
				Flavor flavor = vm.getFlavor();
				String flavorName = flavor.getName();
				log.info("Adding Flavor = " + flavorName);
				vmInfo.add(flavorName);
			}
		}
		log.info("vmInfo size = " + vmInfo.size());
		if(vmInfo.size()==2)
			return vmInfo;
		else
		{
			log.error("No information found for VM = " + vmName);
			return null;
		}
	}
}
