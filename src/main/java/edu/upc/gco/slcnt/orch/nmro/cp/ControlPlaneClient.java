package edu.upc.gco.slcnt.orch.nmro.cp;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.controlplane.interfaces.server.messages.IPoPConnectionRequest;
import edu.upc.gco.slcnt.rest.client.controlplane.interfaces.server.messages.InterDomainConnectionRequest;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;

public class ControlPlaneClient {

	private Logger log = LoggerFactory.getLogger(ControlPlaneClient.class);
	
	private RestTemplate restTemplate;
	
	private String cpServiceUrl;
	
	public ControlPlaneClient (String controlPlaneUrl, RestTemplate restTemplate) {
		
		this.cpServiceUrl = controlPlaneUrl + "/controlplane/services";
		this.restTemplate = restTemplate;
	}
	
	public List<String> createInterPoPConnection (IPoPConnectionRequest request) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		log.debug("Building HTTP request to create Inter PoP Connection.");
		
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> putEntity = new HttpEntity<>(request, header);

		String url = cpServiceUrl + "/IPC/ipop";
		
		try
		{
			log.debug("Sending HTTP request to create Inter PoP Connection.");
			ResponseEntity<List<String>> httpResponse = restTemplate.exchange(url, HttpMethod.PUT, putEntity, new ParameterizedTypeReference<List<String>>() {});
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during Inter PoP Connection creation");
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during Inter PoP Connection creation");
			} else {
				throw new FailedOperationException("Generic error on CP during Inter PoP Connection creation");
			}
		}
		catch (RestClientException e) {
			log.debug("Error while interacting with CP.");
			throw new FailedOperationException("Error while interacting with CP LCM at url " + url);
		}
	}
	
	public String actuateInterPoPConnection (String iPoPCId) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		log.debug("Building HTTP request to actuate over  Inter PoP Connection.");
		
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> putEntity = new HttpEntity<>(header);

		String url = cpServiceUrl + "/IPC/ipop/" + iPoPCId + "/actuate";
		
		try
		{
			log.debug("Sending HTTP request to actuate over Inter PoP Connection.");
			ResponseEntity<String> httpResponse = restTemplate.exchange(url, HttpMethod.PUT, putEntity, String.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during Inter PoP Connection actuation");
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during Inter PoP Connection actuation");
			} else {
				throw new FailedOperationException("Generic error on CP during Inter PoP Connection actuation");
			}
		}
		catch (RestClientException e) {
			log.debug("Error while interacting with CP.");
			throw new FailedOperationException("Error while interacting with CP LCM at url " + url);
		}
	}
	
	public String deleteInterPoPConnection (String iPoPCId) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		log.debug("Building HTTP request to delete Inter PoP Connection.");
		
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> deleteEntity = new HttpEntity<>(header);

		String url = cpServiceUrl + "/IPC/ipop/" + iPoPCId;
		
		try
		{
			log.debug("Sending HTTP request to delete Inter PoP Connection.");
			ResponseEntity<String> httpResponse = restTemplate.exchange(url, HttpMethod.DELETE, deleteEntity, String.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during Inter PoP Connection deletion");
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during Inter PoP Connection deletion");
			} else {
				throw new FailedOperationException("Generic error on CP during Inter PoP Connection deletion");
			}
		}
		catch (RestClientException e) {
			log.debug("Error while interacting with CP.");
			throw new FailedOperationException("Error while interacting with CP LCM at url " + url);
		}
	}
	
	public String createInterDomainConnection (InterDomainConnectionRequest request) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		log.debug("Building HTTP request to create Inter-Domain Connection.");
		
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> putEntity = new HttpEntity<>(request, header);

		String url = cpServiceUrl + "/IDC/connection";
		
		try
		{
			log.debug("Sending HTTP request to create Inter-Domain Connection.");
			ResponseEntity<String> httpResponse = restTemplate.exchange(url, HttpMethod.PUT, putEntity, String.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.CREATED)) {
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during Inter-Domain Connection creation");
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during Inter-Domain Connection creation");
			} else {
				throw new FailedOperationException("Generic error on CP during Inter-Domain Connection creation");
			}
		}
		catch (RestClientException e) {
			log.debug("Error while interacting with CP.");
			throw new FailedOperationException("Error while interacting with CP LCM at url " + url);
		}
	}
}
