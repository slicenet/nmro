package edu.upc.gco.slcnt.orch.nmro.nso;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import edu.upc.gco.slcnt.orch.nmro.core.NetworkService;
import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.core.DCTenant;
import edu.upc.gco.slcnt.orch.nmro.core.Vim;
import edu.upc.gco.slcnt.orch.nmro.eim.EimDriver;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.OpenStackDriverThread;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmDriver;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmTranslator;
import edu.upc.gco.slcnt.orch.nmro.util.SSHClient;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.common.elements.ActuationData;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.common.messages.ActuateServiceRequest;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstance;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstanceId;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.OperationOccurrence;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi.VDUResource;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi.VNFInstance;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.VimInfoObject;
import it.nextworks.nfvmano.libs.ifa.common.enums.InstantiationState;
import it.nextworks.nfvmano.libs.ifa.common.enums.OperationStatus;
import it.nextworks.nfvmano.libs.ifa.common.enums.ResponseCode;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MethodNotImplementedException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.libs.ifa.common.messages.SubscribeRequest;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.NsLcmConsumerInterface;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.NsLcmProviderInterface;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.elements.OperateVnfData;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.enums.NsUpdateType;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.CreateNsIdentifierRequest;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.HealNsRequest;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.InstantiateNsRequest;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.QueryNsResponse;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.ScaleNsRequest;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.TerminateNsRequest;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.UpdateNsRequest;
import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.UpdateNsResponse;
import it.nextworks.nfvmano.libs.ifa.records.nsinfo.NsInfo;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;

/**
 * This service manages the external requests for NS lifecycle management.
 * It interacts with the internal repositories of NS instances for synchronous 
 * actions and sends messages to the engine for asynchronous actions.
 * 
 * Its methods are invoked by the NmroNsLifecycleManagementController class.
 * 
 * @author UPC based on Nextworks
 *
 */
@Service
public class NmroNsLifecycleService implements NsLcmProviderInterface {

	private static final Logger log = LoggerFactory.getLogger(NmroNsLifecycleService.class);
	
	/*@Autowired
	private OsmDriver osmDriver;
	
	@Autowired
	private OsmTranslator osmTranslator;
	@Autowired
	private ApplicationContext appContext;*/
	private Nmro nmro;
	private OsmDriver osmDriver;
	private OsmTranslator osmTranslator;
	//private EimDriver eimDriver;
	
	public NmroNsLifecycleService() {
		
	}
	
	public void initService (Nmro nmro)
	{
		this.nmro = nmro;
		this.osmDriver = nmro.getOsmDriver();
		this.osmTranslator = nmro.getOsmTranslator();
		//this.eimDriver = nmro.getEimDriver();
	}

	@Override
	public String createNsIdentifier(CreateNsIdentifierRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		
		String nsName = request.getNsName();
		String nsdId = request.getNsdId();
		String nsDescription = request.getNsDescription();
		
		DCTenant tenant = (DCTenant)nmro.getTenants().get(request.getTenantId());
		VimInfoObject vio = nmro.selectVimFromTenant(tenant);
		if (vio == null)
		{
			log.error("No VIM found for Tenant '" + request.getTenantId() + "'");
			return null;
		}
		String vimAccountId = vio.getId();
		
		//ResponseEntity<NSInstanceId> response = (ResponseEntity<NSInstanceId>) tenant.getOsmClient().getOsmLcm().createNsiIdentifier(nsName, nsdId, nsDescription, vimAccountId);
		ResponseEntity<?> response = tenant.getOsmClient().getOsmLcm().createNsiIdentifier(nsName, nsdId, nsDescription, vimAccountId);
		
		if (response.getBody().getClass() == NSInstanceId.class)
		{
			String nsiId = ((ResponseEntity<NSInstanceId>)response).getBody().getId();
			
			if (response.getStatusCode() == HttpStatus.CREATED)
			{
				nmro.createNetworkService(request.getTenantId(), vio, nsiId, nsName, nsdId);
			}
			
			//return response.getBody().toString();
			return nsiId;		//SS-O is expecting just the Id not the JSON format
		}
		else
		{
			//TODO: Further error control
			return "";
		}
	}

	@Override
	public String instantiateNs(InstantiateNsRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		
		String nsiId = request.getNsInstanceId();
		NetworkService ns = nmro.findNetworkServiceByNsiId(nsiId);
		if (ns == null)
		{
			log.error("Error trying to initiate NS Instance: Not Found.");
			throw new NotExistingEntityException("NS Instance not found");
		}
		
		//ResponseEntity<OperationOccurrence> response = (ResponseEntity<OperationOccurrence>) ns.getTenant().getOsmClient().getOsmLcm().instantiateNsi(nsiId, ns.getNsName(), ns.getNsdId(), ns.getVim().getId());
		ResponseEntity<?> response = ns.getTenant().getOsmClient().getOsmLcm().instantiateNsi(nsiId, ns.getNsName(), ns.getNsdId(), ns.getVim().getId());
		
		if (response.getBody().getClass() == OperationOccurrence.class)
		{
			String operationId = ((ResponseEntity<OperationOccurrence>) response).getBody().getId();
			ns.setOperationId(operationId);
			
			//TODO: Update NetworkService to INSTANTIATED?
			
			//return response.getBody().toString();
			return operationId;		//SS-O is expecting just the Id not the JSON format
		}
		else
		{
			//TODO: Further error control
			return "";
		}
	}

	@Override
	public String scaleNs(ScaleNsRequest request) throws MethodNotImplementedException, NotExistingEntityException,
			FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UpdateNsResponse updateNs(UpdateNsRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		// TODO Currently used for NS external Network configuration -> Not Standard
		String nsiId = request.getNsInstanceId();
		
		if (!request.getUpdateType().equals(NsUpdateType.OPERATE_VNF))
		{
			log.error("Unsupported NS Update Type");
			throw new FailedOperationException("Unsupported NS Update Type");
		}
		
		NetworkService ns = nmro.findNetworkServiceByNsiId(nsiId);
		
		OperateVnfData networkData = request.getOperateVnfData().get(0);
		
		String level = null, operation = null, segmentType = null;
		HashMap<String,String> parameters = null;
		if (networkData.getAdditionalParam().size() > 3)
			parameters = new HashMap<String,String>();
		log.info("Additonal Params Size = " + networkData.getAdditionalParam().size());
		for (String key : networkData.getAdditionalParam().keySet())
		{
			log.info("Treating key = " + key);
			switch (key)
			{
				case "level":
					level = networkData.getAdditionalParam().get(key);
					break;
				case "operation":
					operation = networkData.getAdditionalParam().get(key);
					break;
				case "segment":
					segmentType = networkData.getAdditionalParam().get(key);
					break;
				default:
					parameters.put(key, networkData.getAdditionalParam().get(key));
					break;
			}
		}
		if (level == null || operation == null | segmentType == null || parameters == null)
		{
			log.error("Something NULL here... Abort");
			return new UpdateNsResponse (nsiId + "-Failed", null, null, null);
		}
		log.info("clientIPs = " + parameters.get("clientIPs"));
		nmro.getCpDriver().createInterDomainConnection(level, operation, segmentType, parameters);
		return new UpdateNsResponse (nsiId + "-1", null, null, null);
	}

	@Override
	public QueryNsResponse queryNs(GeneralizedQueryRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		
		//TODO: Use osmDriver or osmTenantClient? In the latter, the request should carry some information to identify the Tenant
		
		Filter filter = request.getFilter();
		List<String> attributeSelector = request.getAttributeSelector();
		
		List<NsInfo> queryNsResult = new ArrayList<>();
		
		if ((attributeSelector == null) || (attributeSelector.isEmpty()))
		{
			Map<String,String> fp = filter.getParameters();
			if (fp.size() == 0)
			{
				ResponseEntity<List<NSInstance>> response = (ResponseEntity<List<NSInstance>>) osmDriver.getOsmLcm().getNsiInfoList();
				List<NSInstance> nsiList;
		    	nsiList = (List<NSInstance>) response.getBody();
		    	
		    	for (NSInstance nsi : nsiList)
				{
					queryNsResult.add(osmTranslator.FromOsmToNmro(nsi));
				}
			}
			else if ((fp.size() == 1) && (fp.containsKey("NS_ID")))
			{
				ResponseEntity<NSInstance> response = (ResponseEntity<NSInstance>) osmDriver.getOsmLcm().getNsiInfo(fp.get("NS_ID"));
				
				if (response.getStatusCode() == HttpStatus.NOT_FOUND)
					return new QueryNsResponse(ResponseCode.FAILED_NOT_EXISTING_RESOURCE, null);
				
				NSInstance nsi = response.getBody();
				queryNsResult.add(osmTranslator.FromOsmToNmro(nsi));
			}
			else
			{
				log.error("Filter not supported.");
				return new QueryNsResponse(ResponseCode.FAILED_GENERIC, null);
			}
		}
		else
		{
			log.error("Attribute selector not supported.");
			return new QueryNsResponse(ResponseCode.FAILED_GENERIC, null);
		}
		
		return new QueryNsResponse(ResponseCode.OK, queryNsResult);
	}

	@Override
	public String terminateNs(TerminateNsRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		
		String nsiId = request.getNsInstanceId();
		NetworkService ns = nmro.findNetworkServiceByNsiId(nsiId);
		if (ns == null)
		{
			log.error("Error trying to terminate NS Instance: Not Found.");
			throw new NotExistingEntityException("NS Instance not found");
		}
		
		LocalDateTime terminationTime = Instant.ofEpochMilli(request.getTerminateTime().getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
		
		//ResponseEntity<NSInstanceId> response = (ResponseEntity<NSInstanceId>) ns.getTenant().getOsmClient().getOsmLcm().teminateNsi(nsiId, terminationTime);
		ResponseEntity<?> response = ns.getTenant().getOsmClient().getOsmLcm().teminateNsi(nsiId, terminationTime);
		
		if (response != null && response.getBody().getClass() == NSInstanceId.class)
		{
			//TODO: Change NS Instance status to NOT_INSTANTIATED?
			
			//return response.getBody().toString();
			String operationId = ((ResponseEntity<NSInstanceId>) response).getBody().getId();
			// Update the OperationId to the termination operation
			ns.setOperationId(operationId);
			log.info("NS Terminating with Operation Id = " + operationId);
			return operationId;	//SS-O is expecting just the Id not the JSON format
		}
		else
		{
			//TODO: Further error control - Change NS Instance status to FAILED??
			log.error("NS Termination failed with code = " + response.getStatusCode());
			return "";
		}
	}

	@Override
	public void deleteNsIdentifier(String nsInstanceIdentifier) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException {
		
		NetworkService ns = nmro.findNetworkServiceByNsiId(nsInstanceIdentifier);
		if (ns == null)
		{
			log.error("Error trying to delete NS Instance: Not Found.");
			throw new NotExistingEntityException("NS Instance not found");
		}
		
		ResponseEntity<?> response = ns.getTenant().getOsmClient().getOsmLcm().deleteNsInstance(nsInstanceIdentifier);
		log.info("Delete NSI Identifier - Status=" + response.getStatusCodeValue());
		
		if (response.getBody().getClass() == Void.class)
		{
			//if (response.getStatusCode() == HttpStatus.NO_CONTENT)
			// Delete the NetworkService
			nmro.deleteNetworkService(ns);
		}
		
		//TODO: Further error control	
	}

	@Override
	public String healNs(HealNsRequest request) throws MethodNotImplementedException, NotExistingEntityException,
			FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationStatus getOperationStatus(String operationId) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		
		NetworkService ns = nmro.findNetworkServiceByOperationId(operationId);
		if (ns == null)
		{
			log.error("Error trying to access NS Instance: Not Found.");
			throw new NotExistingEntityException("NS Instance not found");
		}
		
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("NS_ID", ns.getNsiId());
		
		ResponseEntity<NSInstance> response = (ResponseEntity<NSInstance>) osmDriver.getOsmLcm().getNsiInfo(parameters.get("NS_ID"));
		
		if (response.getStatusCode() == HttpStatus.NOT_FOUND)
			return OperationStatus.FAILED;
		
		NSInstance nsi = response.getBody();
		switch (nsi.getOperationalStatus())
		{
			case "init":
				return OperationStatus.PROCESSING;
			case "running":
				return OperationStatus.SUCCESSFULLY_DONE;
			case "terminating":
				return OperationStatus.PROCESSING;
			case "terminated":
				return OperationStatus.SUCCESSFULLY_DONE;
			case "failed":
				return OperationStatus.FAILED;
			default:
				log.error("Unhandled Operational Status = " + nsi.getOperationalStatus());
				return null;
		}
	}
	
	@Override
	public String subscribeNsLcmEvents(SubscribeRequest request, NsLcmConsumerInterface consumer)
			throws MethodNotImplementedException, MalformattedElementException, FailedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void unsubscribeNsLcmEvents(String subscriptionId) throws MethodNotImplementedException,
			MalformattedElementException, NotExistingEntityException, FailedOperationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void queryNsSubscription(GeneralizedQueryRequest request) throws MethodNotImplementedException,
			NotExistingEntityException, FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}
	
	// Added by UPC
	public OperationStatus getConfigStatus(String operationId) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException {

		NetworkService ns = nmro.findNetworkServiceByOperationId(operationId);
		if (ns == null)
		{
			log.error("Error trying to access NS Instance: Not Found.");
			throw new NotExistingEntityException("NS Instance not found");
		}
		
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("NS_ID", ns.getNsiId());
		
		ResponseEntity<NSInstance> response = (ResponseEntity<NSInstance>) osmDriver.getOsmLcm().getNsiInfo(parameters.get("NS_ID"));
		
		if (response.getStatusCode() == HttpStatus.NOT_FOUND)
			return OperationStatus.FAILED;
		
		NSInstance nsi = response.getBody();
		switch (nsi.getConfigStatus())
		{
			case "init":
				return OperationStatus.PROCESSING;
			case "configured":
				return OperationStatus.SUCCESSFULLY_DONE;
			case "terminating":
				return OperationStatus.PROCESSING;
			case "terminated":
				return OperationStatus.SUCCESSFULLY_DONE;
			case "failed":
				return OperationStatus.FAILED;
			default:
				log.error("Unhandled Operational Status = " + nsi.getConfigStatus());
				return null;
		}
	}
	
	public ActuationData actuateNs (ActuateServiceRequest request) throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException {
		
		ActuationData actuationData = null;
		
		String nsiId = request.getServiceId();
		log.info("Looking for NsiId = " + request.getServiceId() + " for Actuation = " + request.getActuationName());
		/*NetworkService ns = nmro.findNetworkServiceByNsiId(nsiId);
		if (ns == null)
		{
			log.error("Error trying to actuate over NS Instance: Not Found.");
			throw new NotExistingEntityException("NS Instance not found");
		}*/
		
		ResponseEntity<NSInstance> nsiResponse = (ResponseEntity<NSInstance>) osmDriver.getOsmLcm().getNsiInfo(nsiId);
		
		if (nsiResponse.getStatusCode() == HttpStatus.NOT_FOUND)
			throw new NotExistingEntityException("NS Instance not found");
		
		if (request.getActuationName().equals("Redirect"))
		{
			actuationData = new ActuationData();
			String vnfName = request.getParameters().get("VNF_NAME");
			log.info("VNF_NAME = " + vnfName);
			
			NSInstance nsi = nsiResponse.getBody();
			//Tenant tenant = nmro.getTenants().get(nsi.getAdminObject().getProjectsWrite().get(0));
			DCTenant tenant = nmro.getTenantByProjectId(nsi.getAdminObject().getProjectsWrite().get(0));
			if (tenant == null)
				throw new NotExistingEntityException("Tenant not found");
			
			for (String vnfiId : nsi.getVnfrList())
			{
				log.info("vnfiId = " + vnfiId);
				ResponseEntity<VNFInstance> vnfiResponse = (ResponseEntity<VNFInstance>) osmDriver.getOsmLcm().getVnfiInfo(vnfiId);
				for (VDUResource vdur : vnfiResponse.getBody().getVdurList())
				{
					log.info("vdur = " + vdur.getName());
					if (vdur.getName().equals(vnfName))
					{
						/*log.info("EIM Driver initialized = " + this.eimDriver.isInitialized());
						String vmIp = this.eimDriver.getFloatingIP(vdur.getVimInstanceId());
						log.info("Found IP address = " + vmIp);*/
						String vmIp = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).getFloatingIP(vdur.getVimInstanceId());
						log.info("Found IP address = " + vmIp);
					}
				}
			}
		}
		else if (request.getActuationName().equals("Migrate-Old"))
		{
			actuationData = new ActuationData();
			String vnfId = request.getParameters().get("vnfid");
			String flavorName = request.getParameters().get("flavor");
			log.info("vnfid = " + vnfId + ", flavor = " + flavorName);
			
			// Look for the Tenant but, by what??! the TenantId in the Event? => For the moment assume we have the nsiId
			NSInstance nsi = nsiResponse.getBody();
			//Tenant tenant = nmro.getTenants().get(nsi.getAdminObject().getProjectsWrite().get(0));
			DCTenant tenant = nmro.getTenantByProjectId(nsi.getAdminObject().getProjectsWrite().get(0));
			if (tenant == null)
				throw new NotExistingEntityException("Tenant not found");
			
			String newHostId = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).getValidHost(flavorName);
			
			if (newHostId == null)
				log.error("No server found able to host flavor = " + flavorName);
			else
			{
				// Need to get vmId from vnfId??
				String vmId = "";
				//boolean result = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).migrateVM(vmId, newHostId);
				log.info("Live Migrate VNF = " + vnfId + " to Host = " + newHostId);
			}
		}
		else if (request.getActuationName().equals("Migrate"))
		{
			actuationData = new ActuationData();
			String vmName = request.getParameters().get("host");
			log.info("vmName (host) = " + vmName);
			
			// Look for the Tenant but, by what??! the TenantId in the Event? => For the moment assume we have the nsiId
			NSInstance nsi = nsiResponse.getBody();
			log.info("Number of Tenants for NSI= " + nsi.getAdminObject().getProjectsWrite().size());
			//Tenant tenant = nmro.getTenants().get(nsi.getAdminObject().getProjectsWrite().get(0));
			DCTenant tenant = nmro.getTenantByProjectId(nsi.getAdminObject().getProjectsWrite().get(0));
			if (tenant == null)
				throw new NotExistingEntityException("Tenant not found");
			
			// Get VM Information from OpenStack (vmId, flavor) => Flavor not needed any more since getValidHost gets the host by VM Name directly
			log.info("Getting OS Driver for DC = " + nsi.getDatacenterId());
			ArrayList<String> vmInfo = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).getVmInfo(vmName);
			if (vmInfo == null)
			{
				log.error("No VM Info Found!");
				return null;
			}
			log.info("VM Info = " + vmInfo.get(0) + " / " + vmInfo.get(1));
			String newHostId = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).getValidHost(vmName);
			
			if (newHostId == null)
				log.error("No server found able to host VM = " + vmName);
			else
			{
				// Need to get vmId from vnfId??
				String vmId = vmInfo.get(0);
				log.info("Live Migrate VM = " + vmName + " (" + vmId + ") to Host = " + newHostId);
				boolean result = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).migrateVM(vmId, newHostId);
				if (result)
					log.info("Migration Successful!");
				else
					log.info("Migration Failed!");
			}
		}
		else if(request.getActuationName().equals("Rescale")) {
			actuationData = new ActuationData();
			String vmId = request.getParameters().get("vmId");
			log.info("vmId = " + vmId);
			
			// Look for the Tenant but, by what??! the TenantId in the Event? => For the moment assume we have the nsiId
			NSInstance nsi = nsiResponse.getBody();
			log.info("Number of Tenants for NSI= " + nsi.getAdminObject().getProjectsWrite().size());
			//Tenant tenant = nmro.getTenants().get(nsi.getAdminObject().getProjectsWrite().get(0));
			DCTenant tenant = nmro.getTenantByProjectId(nsi.getAdminObject().getProjectsWrite().get(0));
			if (tenant == null)
				throw new NotExistingEntityException("Tenant not found");
			
			//Scale VM according to parameters cpuScale, memoryScale, storageScale
			
			int cpuScale = Integer.parseInt(request.getParameters().get("cpuScale"));
			int memoryScale = Integer.parseInt(request.getParameters().get("memoryScale"));
			int storageScale = Integer.parseInt(request.getParameters().get("storageScale"));
			log.info("Rescaling VM = " + vmId + "according to parameters cpuScale, memoryScale, storageScale = (" + cpuScale + ", " + memoryScale + ", " + storageScale + ")");
			boolean result = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).rescaleVM(vmId, cpuScale, memoryScale, storageScale);
			if (result)
			{
				log.info("Rescale Successful!");
				//TODO: Test Restart CirrOS scripts
				/*log.info("Re-starting Stress scripts at CirrOS");
				//Get VM Floating IP
				String vmEndPoint = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).getFloatingIP(vmId);
				if (vmEndPoint == null)
					log.error("Could not get Floating IP for VM = " + vmId);
				//Re-start scritps via SSH
				SSHClient sshClient = new SSHClient();
				sshClient.connect("cirros", vmEndPoint, 22, "cubswin:)");
				String cmd = sshClient.buildCommand("StressCirros", null);
				String out = sshClient.executeCommand(cmd);
				sshClient.disconnect();*/
			}
			else
				log.info("Rescale Failed!");
		}
		else if (request.getActuationName().equals("AddIMSIs"))
		{
			actuationData = new ActuationData();
			String imsis = request.getParameters().get("imsis");
			// Get VM associated to NSI from OSM
			NSInstance nsi = nsiResponse.getBody();
			
			//Tenant tenant = nmro.getTenants().get(nsi.getAdminObject().getProjectsWrite().get(0));
			DCTenant tenant = nmro.getTenantByProjectId(nsi.getAdminObject().getProjectsWrite().get(0));
			if (tenant == null)
				throw new NotExistingEntityException("Tenant not found");
			
			VNFInstance epcVnfi = (VNFInstance) osmDriver.getOsmLcm().getVnfiInfo(nsi.getVnfrList().get(0)).getBody();
			if (epcVnfi == null)
				log.info("VNFI NULL");
			else
				log.info("VNFI Vim Account = " + epcVnfi.getVimAccountId());
			VDUResource epcVdur = epcVnfi.getVdurList().get(0);
			String epcVmName = epcVdur.getName();
			Vim vim = tenant.getVimAccounts().get(epcVnfi.getVimAccountId());
			// Check if VIM has Floating IPs
			String epcEndPoint = "";
			if (vim.getOsmVim().getConfig().isUseFloatingIp())
			{
				// Get VM IP end point from OpenStack
				epcEndPoint = ((OpenStackDriverThread)tenant.getVimAccounts().get(nsi.getDatacenterId()).getEimDriver()).getFloatingIP(epcVdur.getVimInstanceId());
				if (epcEndPoint ==null)
					epcEndPoint = epcVdur.getIpAddress();
			}
			else
				epcEndPoint = epcVdur.getIpAddress();
			// Execute oai-cn.hss-add-users for each IMSI of the list
			log.info("SSH to " + epcVmName + " at " + epcEndPoint);
			// Treat IMSI List
			StringTokenizer tokenizer = new StringTokenizer(imsis,",");
			while (tokenizer.hasMoreTokens())
			{
				String imsi = tokenizer.nextToken();
				log.info("Adding IMSI = " + imsi);
				SSHClient sshClient = new SSHClient();
				sshClient.connect("slicenet", epcEndPoint, 22, "slicenet");
				//sshClient.ExecuteAddIMSI(imsi);
				List<String> params = new ArrayList<String>();
				params.add(imsi);
				String cmd = sshClient.buildCommand("AddIMSI", params);
				String out = sshClient.executeCommand(cmd);
				log.debug("output = " + out);
				sshClient.disconnect();
			}
		}
		
		return actuationData;
	}
}
