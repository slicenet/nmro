package edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.elements;

import java.util.ArrayList;
import java.util.List;

import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model.VnfFloatingIpReply;

public class FloatingIpData {

	private List<VnfFloatingIpReply> vnfFloatingIpInfo;
	
	public FloatingIpData()
	{
		this.vnfFloatingIpInfo = new ArrayList<VnfFloatingIpReply>();
	}
	
	public FloatingIpData (List<VnfFloatingIpReply> vnfFloatingIpInfo)
	{
		this.vnfFloatingIpInfo = vnfFloatingIpInfo;
	}

	public List<VnfFloatingIpReply> getVnfFloatingIpInfo() {
		return vnfFloatingIpInfo;
	}
}
