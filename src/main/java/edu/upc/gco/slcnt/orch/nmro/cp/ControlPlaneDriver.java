package edu.upc.gco.slcnt.orch.nmro.cp;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import edu.upc.gco.slcnt.rest.client.common.interfaces.elements.EndPointPair;
import edu.upc.gco.slcnt.rest.client.controlplane.interfaces.server.messages.IPoPConnectionRequest;
import edu.upc.gco.slcnt.rest.client.controlplane.interfaces.server.messages.InterDomainConnectionRequest;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;

@Component
public class ControlPlaneDriver {

	private Logger log = LoggerFactory.getLogger(ControlPlaneDriver.class);
	
	@Value("${cp.proto}")
	private String proto;
	@Value("${cp.ip}")
	private String ipAddress;
	@Value("${cp.port}")
	private String port;
	
	private RestTemplate restTemplate;
	private ControlPlaneClient client;
	
	public ControlPlaneDriver ()
	{
		this.restTemplate = new RestTemplate();
	}
	
	public ControlPlaneClient getClient() {
		return client;
	}

	public void initializeControlPlaneDriver ()
	{
		log.info("Initializing CP Driver");
		this.client = new ControlPlaneClient(this.proto + "://" + this.ipAddress + ":" + this.port, this.restTemplate);
	}
	
	public List<String> createInterPoPConnection (List<EndPointPair> forwardingGraph, HashMap<String,String> parameters) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		IPoPConnectionRequest request = new IPoPConnectionRequest(forwardingGraph, parameters);
		
		List<String> iPoPConnectionIds = this.client.createInterPoPConnection(request);
		
		return iPoPConnectionIds;
	}
	
	public String actuateInterPoPConnection (String iPoPCId) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		return this.client.actuateInterPoPConnection(iPoPCId);
	}
	
	public String deleteInterPoPConnection (String iPoPCId) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		return this.client.deleteInterPoPConnection(iPoPCId);
	}
	
	public String createInterDomainConnection (String level, String operation, String segment, HashMap<String,String> parameters) throws NotExistingEntityException, MalformattedElementException, FailedOperationException
	{
		InterDomainConnectionRequest request = new InterDomainConnectionRequest(level, operation, segment, parameters);
		
		return this.client.createInterDomainConnection(request);
	}
}
