package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.upc.gco.slcnt.rest.client.nmro.interfaces.common.elements.ActuationData;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.common.messages.ActuateServiceRequest;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages.CreateCsIdentifierRequest;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages.InstantiateCsRequest;
import edu.upc.gco.slcnt.rest.client.nmro.interfaces.cslcm.messages.QueryCsResponse;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MethodNotImplementedException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;

@RestController
@RequestMapping("/neto/csLifecycle")
public class NmroCsLifecycleController {

	private Logger log = LoggerFactory.getLogger(NmroCsLifecycleController.class);
	
	@Autowired
	private NmroCsLifecycleService csLcmService;
	
	public NmroCsLifecycleController () { }
	
	@RequestMapping(value = "/cs", method = RequestMethod.POST)
	public ResponseEntity<?> createCsIdentifier (@RequestBody CreateCsIdentifierRequest request)
	{
		log.info ("Received create CS ID request");
		try
		{
			request.isValid();
		}
		catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		try
		{
			String response = csLcmService.createCsIdentifier(request);
			return new ResponseEntity<String>(response, HttpStatus.CREATED);
		}
		catch (MethodNotImplementedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MalformattedElementException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/cs/{csId}/instantiate", method = RequestMethod.PUT)
	public ResponseEntity<?> instantiateCs (@RequestBody InstantiateCsRequest request)
	{
		log.info ("Received instantiate CS request");
		try
		{
			request.isValid();
		}
		catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		try
		{
			String response = csLcmService.instantiateCs(request);
			return new ResponseEntity<String>(response, HttpStatus.OK);
		}
		catch (MethodNotImplementedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MalformattedElementException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/cs/{csId}", method = RequestMethod.GET)
	public ResponseEntity<?> queryCs(@PathVariable String csId)
	{
		log.debug ("Received query CS request");
		
		try
		{
			Map<String,String> parameters = new HashMap<String,String>();
			parameters.put("CS_ID", csId);
			GeneralizedQueryRequest request = new GeneralizedQueryRequest(new Filter(parameters), new ArrayList<>());
			QueryCsResponse response = csLcmService.queryCs(request);
			return new ResponseEntity<QueryCsResponse>(response, HttpStatus.OK);
		}
		catch (MethodNotImplementedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MalformattedElementException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/cs", method = RequestMethod.GET)
	public ResponseEntity<?> queryCs()
	{
		log.debug ("Received query CS request");
		
		try
		{
			Map<String,String> parameters = new HashMap<String, String>();
			GeneralizedQueryRequest request = new GeneralizedQueryRequest(new Filter(parameters), new ArrayList<>());
			QueryCsResponse response = csLcmService.queryCs(request);
			return new ResponseEntity<QueryCsResponse>(response, HttpStatus.OK);
		}
		catch (MethodNotImplementedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MalformattedElementException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/cs/{csId}/terminate", method = RequestMethod.PUT)
	public ResponseEntity<?> terminateCs(@PathVariable String csId)
	{
		log.debug ("Received terminate CS request");
		
		try
		{
			String response = csLcmService.terminateCs(csId);
			return new ResponseEntity<String>(response, HttpStatus.OK);
		}
		catch (MethodNotImplementedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MalformattedElementException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/cs/{csId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCsIdentifier(@PathVariable String csId)
	{
		log.debug ("Received delete CS ID request");
		
		try
		{
			String response = csLcmService.deleteCsIdentifier(csId);
			return new ResponseEntity<String>(response, HttpStatus.OK);
		}
		catch (MethodNotImplementedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MalformattedElementException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/cs/{csId}/actuate", method = RequestMethod.PUT)
	public ResponseEntity<?> actuateCs(@RequestBody ActuateServiceRequest request)
	{
		log.debug ("Received actuate over CS request");
		
		try
		{
			request.isValid();
		}
		catch (MalformattedElementException e)
		{
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		try
		{
			/*ActuationData*/String response = csLcmService.actuateCs(request);
			//return new ResponseEntity<ActuationData>(response, HttpStatus.OK);
			return new ResponseEntity<String>(response, HttpStatus.OK);
		}
		catch (MethodNotImplementedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotExistingEntityException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MalformattedElementException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
