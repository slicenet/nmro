package edu.upc.gco.slcnt.orch.nmro.osm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upc.gco.slcnt.rest.client.osm.lcm.OSMLcmClient;
import it.nextworks.nfvmano.libs.osmr4PlusClient.OSMr4PlusClient;

public class OsmTenantClient {

	private static final Logger log = LoggerFactory.getLogger(OsmTenantClient.class);
	
	private String osmIpAddress;
	private String osmPort;
	private String osmUser;
	private String osmPwd;
	private String osmProject;		// Project associated to the Tenant
	
	private OSMLcmClient osmLcm;
	private OSMr4PlusClient osmCatalogue;
	
	private OsmDriver parent;
	
	public OsmTenantClient ()
	{
		
	}
	
	public OsmTenantClient (OsmDriver parent, String osmIpAddress, String osmPort, String osmUser, String osmPwd, String osmProject)
	{
		this.parent = parent;
		this.osmIpAddress = osmIpAddress;
		this.osmPort = osmPort;
		this.osmUser = osmUser;
		this.osmPwd = osmPwd;
		this.osmProject = osmProject;
		
		this.osmLcm = new OSMLcmClient(osmIpAddress, osmUser, osmPwd, osmProject);
		this.osmCatalogue = new OSMr4PlusClient(osmIpAddress, osmPort, osmUser, osmPwd, osmProject);
	}

	public OSMLcmClient getOsmLcm() {
		return osmLcm;
	}

	public OSMr4PlusClient getOsmCatalogue() {
		return osmCatalogue;
	}
	
	public String getEncrypted (String strToEncrypt, String key)
	{
		return parent.getEncrypted(strToEncrypt, key);
	}
	
	public String getDecrypted (String strToDecrypt, String key)
	{
		return parent.getDecrypted(strToDecrypt, key);
	}
}
