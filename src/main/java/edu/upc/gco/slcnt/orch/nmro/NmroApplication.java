package edu.upc.gco.slcnt.orch.nmro;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmDriver;

@SpringBootApplication
public class NmroApplication implements CommandLineRunner {

	//public OsmDriver osmDriver = new OsmDriver();
	public Nmro nmro = new Nmro();
	
	@Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
        
        nmro.initializeNmro();
	}
	
	public static void main(String[] args) {
		//SpringApplication.run(NmroApplication.class, args);
		new SpringApplication(NmroApplication.class).run(args);
	}
	
	class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }
	
	/*@Bean
	public OsmDriver getOsmDriver()
	{
		return this.osmDriver;
	}*/
	@Bean
	public Nmro getNmro()
	{
		return this.nmro;
	}
}
