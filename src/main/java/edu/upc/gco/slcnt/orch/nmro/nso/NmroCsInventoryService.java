package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import edu.upc.gco.slcnt.orch.nmro.core.ConnectivityService;
import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.cp.ControlPlaneDriver;

@Service
public class NmroCsInventoryService {

	private Logger log = LoggerFactory.getLogger(NmroCsInventoryService.class);
	
	private Nmro nmro;
	private ControlPlaneDriver cpDriver;
	
	public NmroCsInventoryService() { }
	
	public void initService (Nmro nmro)
	{
		this.nmro = nmro;
		this.cpDriver = nmro.getCpDriver();
	}
	
	public List<String> getCsiResources (String csiId)
	{
		List<String> resourceIds = new ArrayList<String>();
		
		ConnectivityService cs = this.nmro.findConnectivityServiceByCsiId(csiId);
		
		if (cs != null)
		{
			for (String iPoPCId : cs.getIpopConnectionIds())
				resourceIds.add(iPoPCId);
		}
		return resourceIds;
	}
}
