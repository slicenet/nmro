package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/neto/inventory")
public class NmroCsInventoryController {

	private Logger log = LoggerFactory.getLogger(NmroCsInventoryController.class);
	
	@Autowired
	private NmroCsInventoryService csInventoryService;
	
	public NmroCsInventoryController() { }
	
	@RequestMapping(value = "/csi/{csiId}", method = RequestMethod.GET)
	public List<String> getCsiResources (@PathVariable String csiId)
	{
		return csInventoryService.getCsiResources(csiId);
	}
}
