package edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.compute.ext.Hypervisor;
import org.openstack4j.model.identity.v3.Token;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpGetValidHostForVm implements Callable<String> {

	private Logger log = LoggerFactory.getLogger(OpGetValidHostForVm.class);
	private Token token;
	private String vmName;
	
	public OpGetValidHostForVm (Token token, String vmName)
	{
		this.token = token;
		this.vmName = vmName;
	}
	
	@Override
	public String call() throws Exception {
		
		OSClientV3 osClient = OSFactory.clientFromToken(token);
		
		/*String currentFlavor = "";
		double vmCPUs = 0;
		double vmRAM = 0;
		
		List<? extends Flavor> flavorList = osClient.compute().flavors().list();
		
		Iterator<? extends Flavor> flavorIt = flavorList.iterator();
		
		while (flavorIt.hasNext() && !currentFlavor.equals(flavorName))
		{
			Flavor f = flavorIt.next();
			currentFlavor = f.getName();
			
			if (currentFlavor.equals(flavorName))
			{
				vmCPUs = f.getVcpus();
				vmRAM = f.getRam();
			}
		}
		
		List<? extends Hypervisor> hypervisorList = osClient.compute().hypervisors().list();
		
		Iterator<? extends Hypervisor> hypervisorIt = hypervisorList.iterator();
		
		while (hypervisorIt.hasNext())
		{
			Hypervisor hypervisor = hypervisorIt.next();
			
			double freeCPUs = hypervisor.getVirtualCPU() - hypervisor.getVirtualUsedCPU();
			double freeRAM = hypervisor.getFreeRam();
			
			if (freeCPUs >= vmCPUs && freeRAM >= vmRAM)
			{
				log.info("Found Destination Server = " + hypervisor.getHypervisorHostname() + " (" + hypervisor.getHostIP() + ")");
				//return hypervisor.getHypervisorHostname();
				return hypervisor.getId();
			}
		}
		
		return null;*/
		
		List<? extends Server> VMs = osClient.compute().servers().list();
		Iterator<? extends Server> it = VMs.iterator();
		
		while(it.hasNext())
		{	
			Server vm = it.next();
			
			String vname = vm.getName();
			
			if(vname.equals(vmName))
			{	
				Flavor flavor = vm.getFlavor();
				
				double vmCPUs = flavor.getVcpus();
				
				double vmRAM = flavor.getRam();
				
				String hvname = vm.getHypervisorHostname();
				
				List<? extends Hypervisor> hypervisors = osClient.compute().hypervisors().list();
				
				Iterator<? extends Hypervisor> itH = hypervisors.iterator();
				
				while(itH.hasNext())
				{	
					Hypervisor hypervisor = itH.next();
					
					String hname = hypervisor.getHypervisorHostname();
					log.info("Checking Hypervisor = " + hname);
					if(!hname.equals(hvname))
					{
						double freeCPUs = hypervisor.getVirtualCPU() - hypervisor.getVirtualUsedCPU();
						
						double freeRAM = hypervisor.getFreeRam();
						log.info("Resources Available = " + freeCPUs + " / " + freeRAM);
						log.info("Resources needed = " + vmCPUs + " / " + vmRAM);
						if(/*freeCPUs>=vmCPUs &&*/ freeRAM>=vmRAM && !hname.equals("h2020-server5"))
							return hypervisor.getHypervisorHostname();
						else
							log.info("Skipping Hypervisor = " + hname);
					}
				}
			}
		}
		
		log.error ("No suitable host found!");
		
		return null;
	}

}
