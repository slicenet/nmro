package edu.upc.gco.slcnt.orch.nmro.osm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import edu.upc.gco.slcnt.orch.nmro.util.AES;
import edu.upc.gco.slcnt.rest.client.osm.lcm.OSMLcmClient;
import it.nextworks.nfvmano.libs.osmr4PlusClient.OSMr4PlusClient;

@Component
public class OsmDriver {

	private static final Logger log = LoggerFactory.getLogger(OsmDriver.class);
	private boolean initialized = false;
	
	@Value("${osm.ip}")
	private String osmIpAddress;
	@Value("${osm.port}")
	private String osmPort;
	@Value("${osm.user}")
	private String osmUser;
	@Value("${osm.password}")
	private String osmPwd;
	@Value("${osm.project}")
	private String osmProject;
	@Value("${osm.serial}")
	private String osmSerial;
	@Value("${osm.commonkey}")
	private String osmCommonKey;
	
	
	private OSMLcmClient osmLcm;
	private OSMr4PlusClient osmCatalogue;
	private AES osmEncryption;
	
	/*@Autowired
	private Environment env;*/
	
	public OsmDriver()
	{
		//initOsmDriver();
	}
	
	public void initOsmDriver ()
	{
		/*String osmIpAddress = env.getProperty("osm.ip");
		String osmUser = env.getProperty("osm.user");
		String osmPwd = env.getProperty("osm.password");
		String osmProject = env.getProperty("osm.project");*/
		
		log.info("Initializing OSM Clients: IP = " + osmIpAddress);
		osmLcm = new OSMLcmClient(osmIpAddress, osmUser, osmPwd, osmProject);
		osmCatalogue = new OSMr4PlusClient(osmIpAddress, osmPort, osmUser, osmPwd, osmProject);
		
		osmEncryption = new AES(osmCommonKey, osmSerial);
		
		initialized = true;
	}
	
	public OSMLcmClient getOsmLcm() {
		return osmLcm;
	}

	public OSMr4PlusClient getOsmCatalogue() {
		return osmCatalogue;
	}

	public boolean isInitialized() {
		return initialized;
	}

	public String getOsmIpAddress() {
		return osmIpAddress;
	}
	
	public String getOsmPort() {
		return osmPort;
	}

	public void setOsmUser(String osmUser) {
		this.osmUser = osmUser;
	}

	public String getOsmProject() {
		return osmProject;
	}

	public String getOsmUser() {
		return osmUser;
	}

	public String getOsmPwd() {
		return osmPwd;
	}
	
	public String getEncrypted (String strToEncrypt, String key)
	{
		return osmEncryption.encrypt(strToEncrypt, key);
	}
	
	public String getDecrypted (String strToDecrypt, String key)
	{
		return osmEncryption.decrypt(strToDecrypt, key);
	}
}
