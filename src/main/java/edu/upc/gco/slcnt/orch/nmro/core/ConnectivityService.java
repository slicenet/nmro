package edu.upc.gco.slcnt.orch.nmro.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import edu.upc.gco.slcnt.rest.client.common.interfaces.elements.EndPoint;
import edu.upc.gco.slcnt.rest.client.common.interfaces.elements.EndPointPair;

public class ConnectivityService {
	
	private NetworkTenant tenant;
	private String csiId;
	private String name;
	private String description;
	
	private List<EndPointPair> forwardingGraph;
	private List<String> ipopConnectionIds;
	
	/*private float latency;
	private int bitrate;
	private boolean protection;
	private String classOfService;*/
	private HashMap<String,String> parameters;

	public ConnectivityService (String name, String description, NetworkTenant tenant)
	{
		this.name = name;
		this.description = description;
		this.tenant = tenant;
		this.csiId = UUID.randomUUID().toString();
	}
	
	public String buildForwardingGraph (List<EndPoint> endPoints)
	{
		String str = "[";
		this.forwardingGraph = new ArrayList<EndPointPair>();
		
		for (int i = 0; i < endPoints.size(); i++)
		{
			for (int j = i+1; j < endPoints.size(); j++ )
			{
				EndPoint endPointA = endPoints.get(i);
				EndPoint endPointB = endPoints.get(j);
				str += "(" + endPointA.getClientIpNetwork() + "," + endPointB.getClientIpNetwork() + ")";
				EndPointPair endPointPair = new EndPointPair (endPointA, endPointB);
				this.forwardingGraph.add(endPointPair);
			}
		}
		
		str += "]";
		return str;
	}
	
	public String getCsiId()
	{
		return csiId;
	}
	
	public NetworkTenant getTenant() {
		return tenant;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public List<String> getIpopConnectionIds() {
		return ipopConnectionIds;
	}

	public List<EndPointPair> getForwardingGraph()
	{
		return forwardingGraph;
	}

	public void setIpopConnectionIds(List<String> ipopConnectionIds) {
		this.ipopConnectionIds = ipopConnectionIds;
	}

	public HashMap<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(HashMap<String, String> parameters) {
		this.parameters = parameters;
	}

	/*public float getLatency() {
		return latency;
	}

	public void setLatency(float latency) {
		this.latency = latency;
	}

	public int getBitrate() {
		return bitrate;
	}

	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}

	public boolean isProtection() {
		return protection;
	}

	public void setProtection(boolean protection) {
		this.protection = protection;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}*/
}
