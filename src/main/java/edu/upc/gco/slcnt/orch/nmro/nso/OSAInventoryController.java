package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.elements.FloatingIpData;
import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.messages.FloatingIpRequest;
import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model.VnfFloatingIpReply;
import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model.VnfFloatingIpRequest;

/**
 * This class implements the REST APIs for the OSA inventory.
 * The internal implementation of the service is implemented through
 * the OSAInventoryService.
 * 
 * @author UPC based on Nextworks
 *
 */
@RestController
@RequestMapping("/nfvo/inventory")
public class OSAInventoryController {

	private static final Logger log = LoggerFactory.getLogger(OSAInventoryController.class);
	
	@Autowired
	OSAInventoryService osaInventoryService;
	
	public OSAInventoryController() { }
	
	@RequestMapping(value = "/getFloatingIps", method = RequestMethod.GET)
	public ResponseEntity<?> queryFloatingIPs(@RequestBody /*FloatingIpRequest*/List<VnfFloatingIpRequest> request) {
		
		/*FloatingIpData*/List<VnfFloatingIpReply> response = osaInventoryService.queryFloatingIPs(request);
		
		return new ResponseEntity<List<VnfFloatingIpReply>>(response, HttpStatus.CREATED);//new ResponseEntity<FloatingIpData>(response, HttpStatus.CREATED);
	}
}
