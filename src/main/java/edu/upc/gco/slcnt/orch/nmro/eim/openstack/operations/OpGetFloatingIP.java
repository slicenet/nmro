package edu.upc.gco.slcnt.orch.nmro.eim.openstack.operations;

import java.util.concurrent.Callable;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.compute.FloatingIP;
import org.openstack4j.model.identity.v3.Token;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upc.gco.slcnt.orch.nmro.eim.openstack.OpenStackDriverThread;

public class OpGetFloatingIP implements Callable<String> {

	private Logger log = LoggerFactory.getLogger(OpGetFloatingIP.class);
	private Token token;
	private String vmId;
	
	public OpGetFloatingIP (Token token, String vmId)
	{
		this.vmId = vmId;
		this.token = token;
	}
	
	@Override
	public String call() throws Exception {
		
		OSClientV3 osClient = OSFactory.clientFromToken(token);
		
		for (FloatingIP floatingIp : osClient.compute().floatingIps().list())
		{
			if (vmId.equals(floatingIp.getInstanceId()))
				return floatingIp.getFloatingIpAddress();
		}
		
		return null;
	}
}
