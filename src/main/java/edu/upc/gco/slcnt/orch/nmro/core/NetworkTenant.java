package edu.upc.gco.slcnt.orch.nmro.core;

import java.util.HashMap;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetworkTenant extends Tenant {

	private Logger log = LoggerFactory.getLogger(NetworkTenant.class);
	
	private HashMap<String, ConnectivityService> connectivityServices;
	
	public NetworkTenant ()
	{
		
	}
	
	public NetworkTenant (int localId, String name)
	{
		this.localId = localId;
		this.id = UUID.randomUUID();
		this.name = name;
		
		this.connectivityServices = new HashMap<String, ConnectivityService>();
	}

	public HashMap<String, ConnectivityService> getConnectivityServices() {
		return connectivityServices;
	}
}
