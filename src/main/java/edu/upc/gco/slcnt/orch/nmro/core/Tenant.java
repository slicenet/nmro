package edu.upc.gco.slcnt.orch.nmro.core;

import java.util.UUID;

public abstract class Tenant {

	protected UUID id;											// Tenant Id (UUID) at NMR-O
	protected int localId;										// Local Numerical Id at NMR-O
	protected String name;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public int getLocalId() {
		return localId;
	}

	public void setLocalId(int localId) {
		this.localId = localId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
