package edu.upc.gco.slcnt.orch.nmro.nso;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import edu.upc.gco.slcnt.orch.nmro.core.Nmro;
import edu.upc.gco.slcnt.orch.nmro.core.Tenant;
import edu.upc.gco.slcnt.orch.nmro.core.DCTenant;
import edu.upc.gco.slcnt.orch.nmro.core.Vim;
import edu.upc.gco.slcnt.orch.nmro.eim.openstack.OpenStackDriverThread;
import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.elements.FloatingIpData;
import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.messages.FloatingIpRequest;
import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model.VnfFloatingIpReply;
import edu.upc.gco.slcnt.orch.nmro.nso.interfaces.osa.model.VnfFloatingIpRequest;
import edu.upc.gco.slcnt.orch.nmro.osm.OsmDriver;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi.VDUResource;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.vnfi.VNFInstance;

@Service
public class OSAInventoryService {

	private static final Logger log = LoggerFactory.getLogger(OSAInventoryService.class);
	
	private Nmro nmro;
	private OsmDriver osmDriver;
	
	public OSAInventoryService()
	{
		
	}
	
	public void initService (Nmro nmro)
	{
		this.nmro = nmro;
		this.osmDriver = nmro.getOsmDriver();
	}
	
	public /*FloatingIpData*/List<VnfFloatingIpReply> queryFloatingIPs (/*FloatingIpRequest request*/List<VnfFloatingIpRequest> request)
	{
		List<VnfFloatingIpReply> replyList = new ArrayList<VnfFloatingIpReply>();
		
		//ResponseEntity<List<VNFInstance>> response = (ResponseEntity<List<VNFInstance>>)osmDriver.getOsmLcm().getVnfiInfoList();
		//log.info("Response = " + response.getBody().size());
		for (VnfFloatingIpRequest vnfInfoReq : request/*.getVnfList()*/)
		{
			String vmId = vnfInfoReq.getVnfVmId();
			log.info("vmId = " + vmId);
			/*for (VNFInstance vnfi : response.getBody())
			{
				log.info("vnfi = " + vnfi.getId());
				for (VDUResource vdur : vnfi.getVdurList())
				{
					log.info("vdur = " + vdur.getVimInstanceId());
					if (vmId.equals(vdur.getVimInstanceId()))
					{
						for (Tenant tenant : nmro.getTenants().values())
						{
							Vim vim = tenant.getVimAccounts().get(vnfInfoReq.getVimAccountId());
							log.info("vimId = " + vnfInfoReq.getVimAccountId());
							if (vim != null)
							{
								log.info("found!");
								String floatingIp = ((OpenStackDriverThread) vim.getEimDriver()).getFloatingIP(vmId);
								log.info("Floating IP = " + floatingIp);
								replyList.add(new VnfFloatingIpReply (vmId, vnfInfoReq.getVimAccountId(), floatingIp));
								break;
							}
						}
						break;
					}
				}
			}*/
			
			for (Tenant tenant : nmro.getTenants().values())
			{
				Vim vim = ((DCTenant)tenant).getVimAccounts().get(vnfInfoReq.getVimAccountId());
				log.info("vimId = " + vnfInfoReq.getVimAccountId());
				if (vim != null)
				{
					log.info("found!");
					String floatingIp = ((OpenStackDriverThread) vim.getEimDriver()).getFloatingIP(vmId);
					log.info("Floating IP = " + floatingIp);
					replyList.add(new VnfFloatingIpReply (vmId, vnfInfoReq.getVimAccountId(), floatingIp));
					break;
				}
			}
		}
		
		return replyList/*new FloatingIpData(replyList)*/;
	}
}
