# Network Domain Resource Orchestrator for SliceNet Project: NMR-O

Spring Boot Application


## Overview  

At NSP level, the NMR-O interfaces the Slice Orchestrator (SS-O) with the NFV-O (Open Source MANO)
and the VIM (OpenStack) to configure the virtual and physical network infrastructure associated to
a Slice.   

## Build

To compile the library use the command:

$ mvn compile

To package the library into a jar file use the command:

$ mvn package

To install the library in the local repository use the command:

$ mvn install

